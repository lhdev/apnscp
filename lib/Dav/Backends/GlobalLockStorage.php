<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Dav\Backends;

	use Sabre\DAV as Provider;
	use Sabre\DAV\Locks\LockInfo;

	class GlobalLockStorage extends Provider\Locks\Backend\PDO
	{
		// clean up apnscp db
		const SITE_SEPARATOR = '.';
		public $tableName = 'dav_locks';

		/**
		 * Returns a list of Sabre\DAV\Locks\LockInfo objects
		 *
		 * This method should return all the locks for a particular uri, including
		 * locks that might be set on a parent uri.
		 *
		 * If returnChildLocks is set to true, this method should also look for
		 * any locks in the subtree of the uri for locks.
		 *
		 * @param string $uri
		 * @param bool   $returnChildLocks
		 * @return array
		 */
		public function getLocks($uri, $returnChildLocks)
		{
			$locks = parent::getLocks(
				$this->normalize($uri),
				$returnChildLocks
			);
			foreach ($locks as $l) {
				$l->uri = $this->denormalize($l->uri);
			}

			return $locks;
		}

		protected function normalize($path)
		{
			if ($_SESSION['level'] & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				return 'site' . $_SESSION['site_id'] . self::SITE_SEPARATOR . $path;
			}

			return $path;
		}

		protected function denormalize($path)
		{
			if ($_SESSION['level'] & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				return substr($path, strpos($path, self::SITE_SEPARATOR) + 1);
			}

			return $path;
		}

		/**
		 * Locks a uri
		 *
		 * @param string   $uri
		 * @param LockInfo $lockInfo
		 * @return bool
		 */
		public function lock($uri, LockInfo $lockInfo)
		{
			return parent::lock(
				$this->normalize($uri),
				$lockInfo
			);
		}

		/**
		 * Removes a lock from a uri
		 *
		 * @param string   $uri
		 * @param LockInfo $lockInfo
		 * @return bool
		 */
		public function unlock($uri, LockInfo $lockInfo)
		{
			return parent::unlock(
				$this->normalize($uri),
				$lockInfo
			);
		}
	}