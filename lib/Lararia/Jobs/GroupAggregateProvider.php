<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs;

	use Illuminate\Queue\Events\JobProcessed;
	use Illuminate\Support\Collection;
	use Illuminate\Support\Facades\Event;
	use Illuminate\Support\ServiceProvider;
	use Lararia\Jobs\Contracts\GroupableInterface;
	use Lararia\Jobs\Notifications\GroupFinished;
	use Lararia\Jobs\Notifications\JobFinished;
	use Laravel\Horizon\Contracts\TagRepository;
	use Laravel\Horizon\Events\JobFailed;
	use Laravel\Horizon\Events\JobPushed;

	class GroupAggregateProvider extends ServiceProvider
	{
		public function boot()
		{
			Event::listen([JobProcessed::class, JobFailed::class], static function ($event) {
				$job = JobExtractor::extractJobCommand($event);
				$id = (int)array_get(JobExtractor::extractRawPayload($event), 'id', 0);
				$job->setJob($event->job);
				$job->setId($id);
				if (!method_exists($job, 'getGroupTag')) {
					/**
					 * @todo move to separate listener channel
					 */
					Event::dispatch(JobFinished::class, $job, $job->getView());

					return true;
				}

				debug('Finished job %(id)d, group: %(group)s',
					['id' => $job->getJobId(), 'group' => $job->getGroupTag()]
				);

				// @XXX Redis can buffer a query resulting in incorrect information
				// sleep for 250ms?
				// 2018/02/24 -> 750 ms, drains too fast on individual WP plugin updates
				// 2018/04/19 -> 1.25 s
				usleep(1250000);

				if ($job->hasMore()) {
					debug('Remaining jobs: [%(jobs)s], group: %(group)s ',
						[
							'jobs' => implode(', ', collect($job->getReservedJobs())->concat($job->getPendingJobs())->pluck('id')->toArray()),
							'group' => $job->getGroupTag()
						]
					);

					return false;
				}

				debug('Group %(group)s done. Jobs [%(jobs)s]',
					[
						'group' => $job->getGroupTag(),
						'jobs' => implode(', ', collect($job->getCompletedJobs())->concat($job->getFailedJobs())->pluck('id')->toArray())
					]
				);
				$jobs = collect($job->getFailedJobs())->concat($job->getCompletedJobs());
				Event::dispatch(GroupFinished::class, $jobs->count() ? $jobs : collect());

				return true;
			});

			Event::listen(JobFinished::class, static function (Job $job) {

				(new Mailer($job))->send();
			});

			Event::listen(GroupFinished::class, static function (Collection $jobs) {
				if (!$jobs->count()) {
					warn('queue fired too fast - lost a job');
					return;
				}

				$head = JobExtractor::extractJobCommand($jobs->first());
				if ($head instanceof GroupableInterface) {
					$tagrepo = resolve(TagRepository::class);
					debug("Forgetting jobs [%s]", implode(', ', $jobs->pluck('id')->toArray()));
					$tagrepo->forgetJobs($head->getGroupTag(), $jobs->pluck('id')->toArray());
				}

				(new Mailer($jobs))->send();
			});

			Event::listen(JobPushed::class, static function (JobPushed $event) {
				$job = JobExtractor::extractJobCommand($event);
				if (!method_exists($job, 'appendGroupToTag')) {
					return true;
				}

				return $job->appendGroupToTag($event);
			});
		}
	}

