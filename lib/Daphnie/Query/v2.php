<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2021
 */


namespace Daphnie\Query;

class v2 extends Generic
{
	public function hasCompression(): string
	{
	    return "SELECT number_compressed_chunks FROM hypertable_compression_stats('metrics') WHERE number_compressed_chunks > 0";
	}

	public function getJobs(): string
	{
		return "SELECT * FROM timescaledb_information.job_stats WHERE hypertable_schema = 'metrics'";
	}

	public function databaseUsage(): string
	{
		return "SELECT ht.id,
			ht.schema_name AS table_schema,
			ht.table_name,
			t.tableowner AS table_owner,
			ht.num_dimensions,
			( SELECT count(1) AS countf
				   FROM _timescaledb_catalog.chunk ch
				  WHERE ch.hypertable_id = ht.id) AS num_chunks,
			bsize.table_bytes,
			bsize.index_bytes,
			bsize.toast_bytes,
			bsize.total_bytes
			FROM _timescaledb_catalog.hypertable ht
			 LEFT JOIN pg_tables t ON ht.table_name = t.tablename AND ht.schema_name = t.schemaname
			 LEFT JOIN LATERAL hypertable_detailed_size(
				CASE
					WHEN has_schema_privilege(ht.schema_name::text, 'USAGE'::text) THEN format('%I.%I'::text, ht.schema_name, ht.table_name)
					ELSE NULL::text
				END::regclass) bsize(table_bytes, index_bytes, toast_bytes, total_bytes) ON true
			WHERE table_name = 'metrics'";
	}

	public function disableArchivalCompression(): string
	{
		return "SELECT remove_compression_policy('metrics');";
	}

	public function getChunkCompressionStats(string $chunk): string
	{
		$chunk = $this->canoncializeChunk($chunk);
		$chunkSQL = $this->db->quote($chunk);

		return "SELECT * FROM chunk_compression_stats('metrics') WHERE chunk_name = ${chunkSQL} LIMIT 1";
	}

	public function getCompressionStats(): string
	{
		return "SELECT 
			total_chunks,
			number_compressed_chunks,
		 	before_compression_table_bytes AS uncompressed_table_bytes,
		 	before_compression_index_bytes AS uncompressed_index_bytes,
		 	before_compression_toast_bytes AS uncompressed_toast_bytes,
		 	before_compression_total_bytes AS uncompressed_total_bytes,
		 	after_compression_table_bytes AS compressed_table_bytes,
		 	after_compression_index_bytes AS compressed_index_bytes,
		 	after_compression_toast_bytes AS compressed_toast_bytes,
		 	after_compression_total_bytes AS compressed_total_bytes
		 FROM hypertable_compression_stats('metrics')";
	}

	public function isCompressed(string $chunk): string
	{
		$chunk = $this->canoncializeChunk($chunk);

		return "SELECT 1 FROM chunk_compression_stats('metrics') " .
			"WHERE compression_status = 'Compressed' AND chunk_name = " . $this->db->quote($chunk);
	}


	public function getChunkStats(string $chunk = null): string
	{
		$chunkQuery = '';
		if ($chunk) {
			[$schema, $table] = explode('.', $chunk, 2);
			$chunkQuery = "AND c.schema_name = " . $this->db->quote($schema) .
				" AND c.table_name = " . $this->db->quote($table);
		}

		return "WITH cte AS (
			SELECT relname, nspname
			FROM pg_class c
			INNER JOIN pg_namespace n ON (n.OID = c.relnamespace)
			WHERE c.OID = 'metrics'::regclass
		)
      
        SELECT chunk_id,
        chunk_table,
        partitioning_columns,
        partitioning_column_types,
        partitioning_hash_functions,
        ranges,
        table_bytes,
        index_bytes,
        toast_bytes,
        total_bytes
        FROM (
        SELECT *,
              total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes
              FROM (
               SELECT c.id as chunk_id,
               format('%I.%I', c.schema_name, c.table_name) as chunk_table,
               pg_total_relation_size(format('%I.%I', c.schema_name, c.table_name)) AS total_bytes,
               pg_indexes_size(format('%I.%I', c.schema_name, c.table_name)) AS index_bytes,
               pg_total_relation_size(reltoastrelid) AS toast_bytes,
               array_agg(d.column_name ORDER BY d.interval_length, d.column_name ASC) as partitioning_columns,
               array_agg(d.column_type ORDER BY d.interval_length, d.column_name ASC) as partitioning_column_types,
               array_agg(d.partitioning_func_schema || '.' || d.partitioning_func ORDER BY d.interval_length, d.column_name ASC) as partitioning_hash_functions,
               array_agg(int8range(range_start, range_end) ORDER BY d.interval_length, d.column_name ASC) as ranges
               FROM
               _timescaledb_catalog.hypertable h,
               _timescaledb_catalog.chunk c,
               _timescaledb_catalog.chunk_constraint cc,
               _timescaledb_catalog.dimension d,
               _timescaledb_catalog.dimension_slice ds,
               pg_class pgc,
               pg_namespace pns
               WHERE pgc.relname = c.table_name
                     AND pns.oid = pgc.relnamespace
                     AND pns.nspname = c.schema_name
                     AND relkind = 'r'
                     AND c.hypertable_id = h.id
                     AND c.id = cc.chunk_id
                     AND cc.dimension_slice_id = ds.id
                     AND ds.dimension_id = d.id
                     $chunkQuery
               GROUP BY c.id, pgc.reltoastrelid, pgc.oid ORDER BY c.id
               ) sub1
        ) sub2";
	}

	public function resumeJob(int $jobid): string
	{
		return "SELECT alter_job(${jobid}, next_start=>now())";
	}

	public function pauseJob(int $jobid): string
	{
		return "SELECT alter_job(${jobid}, next_start=>'infinity');";
	}

	public function refreshContinuousAggregate(string $view): string
	{
		return "CALL refresh_continuous_aggregate(" . $this->db->quote($view) . ", NULL, NULL)";
	}
}