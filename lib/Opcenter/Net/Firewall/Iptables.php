<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2018
	 */


	namespace Opcenter\Net\Firewall;

	/**
	 * Class Firewall
	 *
	 * A simple firewall to block incoming connections
	 *
	 * @package Opcenter\Net
	 */
	class Iptables
	{
		const COMMAND = 'iptables -w 30';
		const TARGETS = [
			'REJECT',
			'ACCEPT',
			'DROP'
		];

		/**
		 * Get all firewall chains
		 *
		 * @return array
		 */
		public static function chains(): array
		{
			$chains = [];
			$cmd = \Util_Process::exec(self::COMMAND . ' --list-rules');
			$tok = strtok($cmd['stdout'], "\n");
			do {
				if (0 !== strncmp($tok, '-N ', 3)) {
					continue;
				}
				$chains[] = substr($tok, 3);

			} while (false !== ($tok = strtok("\n")));

			return $chains;
		}

		/**
		 * Get IP entries from chains
		 *
		 * @param null|string $chain optional chain to filter
		 * @return array
		 */
		public static function getEntriesFromChain(string $chain = null): array
		{
			$cmd = \Util_Process_Safe::exec(self::COMMAND . ' --line-numbers -n -L %s', $chain);
			if (!$cmd['success']) {
				error("failed to get entries from chain `%s': %s", $chain, $cmd['stderr']);

				return [];
			}
			if (null === $chain) {
				$all = [];
			}
			$entries = [];
			$tok = strtok($cmd['stdout'], "\n");
			do {
				$line = preg_split('/\s+/', $tok, 7);
				if (null === $chain && $line[0] === 'Chain') {
					unset($entries);
					$entries = [];
					$all[$line[1]] = &$entries;
				}
				if (!\in_array($line[1], static::TARGETS, true)) {
					continue;
				}
				$entries[] = [
					'record'      => (int)$line[0],
					'target'      => $line[1],
					'proto'       => $line[2],
					'opt'         => $line[3] === '--' ? null : $line[3],
					'source'      => $line[4],
					'destination' => $line[5],
					'type'        => trim($line[6]),
				];

			} while (false !== ($tok = strtok("\n")));

			return $chain ? $entries : $all;
		}

		/**
		 * Append a rule to the end of a chain
		 *
		 * @param string      $chain
		 * @param string      $ip
		 * @param string      $target
		 * @param string|null $action
		 * @return bool
		 */
		public static function append(string $chain, string $ip, string $target, string $action = null): bool
		{
			if (!\in_array($target, static::TARGETS, true)) {
				return error("unknown target `%s'", $target);
			}
			$cmd = \Util_Process_Safe::exec(self::COMMAND . ' -A %s -s %s -j %s ' . array_map('escapeshellarg',
					explode(' ', $action)),
				$chain, $ip, $target, $action
			);

			return $cmd['success'] ?: error('failed to append rule %s with source %s to %s: %s', $target, $ip, $chain,
				$cmd['stderr']);
		}

		/**
		 * Insert a rule to the beginning of a chain
		 *
		 * @param string      $chain
		 * @param string      $ip
		 * @param string      $target
		 * @param string|null $action
		 * @return bool
		 */
		public static function insert(string $chain, string $ip, string $target, string $action = null): bool
		{
			if (!\in_array($target, static::TARGETS, true)) {
				return error("unknown target `%s'", $target);
			}
			$cmd = \Util_Process_Safe::exec(self::COMMAND . ' -I %s -s %s -j %s ' . array_map('escapeshellarg',
					explode(' ', $action)),
				$chain, $ip, $target, $action
			);

			return $cmd['success'] ?: error("failed to insert rule `%s' with source `%s' to `%s': %s", $target, $ip,
				$chain,
				$cmd['stderr']);
		}

		/**
		 * Remove rule by index
		 *
		 * @param string $chain
		 * @param int    $ruleno
		 * @return bool
		 */
		public static function remove(string $chain, int $ruleno): bool
		{
			$ret = \Util_Process_Safe::exec(self::COMMAND . ' -D %s %d', $chain, $ruleno);

			return $ret['success'] ?: error("failed to remove rule from `%s' indexed at pos %d: %s", $chain, $ruleno,
				$ret['stderr']);
		}
	}