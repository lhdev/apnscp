<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Headless implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg['panel_headless'] = (bool)$val;
			unset($cfg);
			Bootstrapper::run('apnscp/bootstrap', 'network/setup-firewall', 'software/argos');

			return true;
		}

		public function get()
		{
			return APNSCPD_HEADLESS;
		}

		public function getHelp(): string
		{
			return 'Disable apnscp frontend';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}