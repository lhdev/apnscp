<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Argos;

	use Opcenter\Admin\Settings\SettingsInterface;

	class Backends implements SettingsInterface
	{
		use \apnscpFunctionInterceptorTrait;

		public function __construct()
		{
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login())));
		}


		public function get()
		{
			return $this->getApnscpFunctionInterceptor()->argos_get_backends();
		}

		public function set($val): bool
		{
			return error('This option is immutable. Use argos.backend or argos.backend-high to set backends');
		}

		public function getHelp(): string
		{
			return 'View Argos backends';
		}

		public function getValues()
		{
			return $this->getApnscpFunctionInterceptor()->argos_get_backends();
		}

		public function getDefault()
		{
			return ['default', 'high'];
		}
	}