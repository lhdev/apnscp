<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */

	namespace Opcenter\Admin\Settings\Apache;

	class InsecureSsl extends SystemDirective
	{
		const DIRECTIVE = 'INSECURE_SSL';

		public function get(...$val)
		{
			return parent::get(self::DIRECTIVE);
		}

		public function set($val, ...$x): bool
		{
			return parent::set(self::DIRECTIVE, $val);
		}

		public function getHelp(): string
		{
			return 'Enable legacy TLS v1.0 and v1.1 support';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}