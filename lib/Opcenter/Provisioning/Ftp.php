<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\MapperTrait;
	use Opcenter\Service\Validators\Ftp\Ftpserver;

	/**
	 * Class Siteinfo
	 *
	 * @package Opcenter\Provisioning
	 */
	class Ftp
	{
		use MapperTrait;
		use FilesystemPopulatorTrait;

		const SUPPLEMENTAL_GROUPS = [
			'ftp' => ['ftp'],
		];

		const TEMPLATE_DIRECTORIES = [
			'/etc/vsftpd' => ['root', 'root', 0755],
		];

		const TEMPLATE_FILES = [
			'/etc/vsftpd.chroot_list' => ['root', 'root', 0644]
		];

		const MAP_FILE = Ftpserver::MAP_FILE;

	}