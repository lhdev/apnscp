<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Contracts\Subscriber;
	use Event\Events;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;

	/**
	 * Class ReconfigurationCallback
	 *
	 * Account editor wrapper
	 *
	 * @package Opcenter\Service
	 */
	class ReconfigurationCallback implements Subscriber
	{
		protected $validator;
		protected $ctx;
		protected $cfgvar;

		public function __construct(ServiceValidator $validator, ConfigurationContext $ctx, string $cfgvar)
		{
			$this->validator = $validator;
			$this->ctx = $ctx;
			$this->cfgvar = $cfgvar;
		}

		/**
		 * @param string    $event
		 * @param Publisher $caller
		 * @return bool
		 */
		public function update($event, Publisher $caller): bool
		{
			$module = $this->ctx->getModuleName();
			if (Cardinal::is($event, Events::SUCCESS)) {
				$fn = 'reconfigure';
			} else {
				$fn = 'rollback';
			}
			if (!($this->validator instanceof ServiceExplicitReconfiguration) &&
				$this->ctx->getOldServiceValue($module, 'enabled') !== $this->ctx->getNewServiceValue($module, 'enabled'))
			{
				$switchlabel = $this->ctx->getNewServiceValue($module, 'enabled') ? 'enablement' : 'disablement';
				debug('%s,%s module implements %s. %s detected, skipping %s', $module, $this->cfgvar, $fn, $switchlabel, $fn);
				return true;
			}
			debug('Running %s on %s,%s', $fn, $module, $this->cfgvar);
			$old = $this->ctx->getOldServiceValue($module, $this->cfgvar);
			$new = $this->ctx->getNewServiceValue($module, $this->cfgvar);
			if (!$this->validator->$fn($old, $new, $caller)) {
				return false;
			}

			return true;
		}
	}