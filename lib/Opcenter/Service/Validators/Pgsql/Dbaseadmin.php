<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Pgsql;

	use Opcenter\Auth\Password;
	use Opcenter\Database\PostgreSQL;
	use Opcenter\SiteConfiguration;

	class Dbaseadmin extends \Opcenter\Service\Validators\Mysql\Dbaseadmin
	{
		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$this->ctx['enabled']) {
				return true;
			}
			if (!parent::reconfigure($old, $new, $svc)) {
				return false;
			}
			if (!$new || $new === $old) {
				// disabled
				return true;
			}
			$afi = $svc->getSiteFunctionInterceptor();
			if (!($pass = $afi->pgsql_get_password($new))) {
				if ($this->ctx->isEdit()) {
					warn('Changing username clears PostgreSQL passsword. A new password must be set.');
				}
				$pass = Password::generate(16);
			}

			if ($old) {
				// already changed in parent call
				if (($tblspace = PostgreSQL::getTablespaceFromUser($old))) {
					// ALTER ROLE updates tablespace ownership
					if (!PostgreSQL::changeTablespaceOwner($tblspace, $new)) {
						return error('Failed to change tablespace owner');
					}
				}
			}

			return $afi->pgsql_edit_user($new, $pass);
		}

	}