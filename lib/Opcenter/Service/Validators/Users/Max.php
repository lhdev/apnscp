<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Users;

	use Opcenter\Service\ServiceValidator;

	class Max extends ServiceValidator
	{
		const DESCRIPTION = 'Limit up to # secondary users';
		const VALUE_RANGE = '[null, 0-4096]';

		public function valid(&$value): bool
		{
			if ($value === null) {
				return true;
			}

			if (!$this->ctx->getServiceValue(null, 'enabled')) {
				$value = 0;

				return true;
			}

			$value = (int)$value;
			if ($value < 1 || $value > 1024) {
				return error('user limit must be between 1 and 1024');
			}

			return true;
		}

	}

