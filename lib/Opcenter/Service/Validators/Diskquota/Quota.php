<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Diskquota;

	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Quota extends ServiceValidator implements ServiceReconfiguration
	{
		const DESCRIPTION = 'Account storage quota';

		public function valid(&$value): bool
		{
			if (!$value) {
				$value = null;
				return true;
			}

			if (!is_numeric($value)) {
				return error("quota threshold must be numeric, `%s' given", $value);
			}

			$hasAmnesty = $this->ctx['amnesty'];
			if ($hasAmnesty && $hasAmnesty !== $this->ctx->getNewServiceValue(null, 'amnesty')
				&& !empty($this->ctx->getNewServiceValue('diskquota', 'quota')))
			{
				// quota set independent of amnesty, clear amnesty flag
				// as storage has been permanently upgraded
				warn('diskquota changed, amnesty flag cleared');

				$this->ctx->set('amnesty', null);
			}

			$units = $this->ctx->getServiceValue('diskquota', 'units');
			$tmp = \Formatter::changeBytes($value, 'B', $units);
			if ($tmp < 1) {
				return error("diskquota must be a non-negative number, `%s' found", $value);
			}

			if (!$this->checkQuota($value)) {
				return false;
			}

			return true;
		}


		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old) {
				return true;
			}

			if ($this->ctx->isEdit()) {
				$this->cancelAmnesty($svc);
			}

			return true;
		}

		public function cancelAmnesty(SiteConfiguration $svc): void
		{
			$job = (new \Util_Process_Schedule)->
				setID(\Site_Module::AMNESTY_JOB_MARKER, $svc->getAuthContext())->
				idPending(\Site_Module::AMNESTY_JOB_MARKER, $svc->getAuthContext());
			if (!$job) {
				return;
			}

			info('Canceling amnesty reset scheduled for %s', date('r', $job['ts']));
			(new \Util_Process_Schedule)->cancelJob($job['job']);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		/**
		 * Validate if change will put site over usage
		 *
		 * @param null|float $quota
		 * @return bool
		 */
		private function checkQuota(?float $quota) {
			if (!$quota || !$this->ctx['enabled'] || !$this->ctx->isEdit()) {
				return true;
			}
			$newquota = (int)\Formatter::changeBytes(
				$quota,
				'KB',
				$this->ctx->getServiceValue(null, 'units')
			);
			$usage = array_get(\Opcenter\Filesystem\Quota::getGroup($this->ctx->getServiceValue('siteinfo', 'admin')), 'qused', 0);
			if ($usage > $newquota) {
				if (!$this->ctx->getConfigurationContainer()->hasValidatorOption('force')) {
					return error("Storage usage will exceed diskquota,quota by %d KB. Rejecting change without --force.", $usage - $newquota);
				}
				return warn("Storage usage will exceed diskquota,quota by %d KB", $usage - $newquota);
			}

			return true;
		}

		public function getValidatorRange()
		{
			// @todo get available storage from device∞
			return '[null,0-∞]';
		}

	}