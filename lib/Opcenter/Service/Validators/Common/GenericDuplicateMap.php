<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Common;

	abstract class GenericDuplicateMap extends GenericMap
	{
		use \NamespaceUtilitiesTrait;
		const MAP_FILE = null;

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue(null, 'enabled')) {
				$value = null;

				return true;
			}
			if (!parent::valid($value)) {
				return false;
			}

			return true;
		}

	}
