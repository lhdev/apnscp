<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter;

	use CLI\Output;
	use Illuminate\Support\Str;
	use Opcenter\Service\ConfigurationContext;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Plans;
	use Opcenter\Service\ServiceValidator;

	class CliParser
	{
		/**
		 * Ignore legacy "reseller" service
		 */
		const HIDE_RESELLER = true;

		public static function parse(): array
		{
			$options = [
				'fd' => Output::STDOUT
			];
			if ($_SERVER['argc'] < 2) {
				static::help();
			}

			$optind = null;
			$longopts = [
				'force',
				'all',
				'help',
				'notify',
				'bootstrap::',
				'version',
				'output:',
				'plan:',
				'since:',
				'dry-run',
				'reconfig',
				'reset',
				'backup',
				'reason:',
				'template:',
				'match:',
				'fd:'
			];
			foreach (getopt('p:Vvfc:ho:an', $longopts,
				$optind) as $opt => $val) {
				switch ($opt) {
					case 'V':
					case 'version':
						echo \Opcenter::version(), PHP_EOL, PHP_EOL;
						exit(0);
					case 'f':
					case 'force':
						$options['force'] = true;
						break;
					case 'a':
					case 'all':
						$options['all'] = true;
						break;
					case 'reconfig':
						$options['reconfig'] = true;
						break;
					case 'help':
					case 'h':
						static::help();
					case 'fd':
						$fd = (int)$val;
						if ($fd < 1 || $fd >= PHP_FD_SETSIZE) {
							fatal("File descriptor %d out of bounds", $fd);
						}
						$options['fd'] = $fd;
						break;
					case 'output':
					case 'o':
						if ($val !== 'json' && $val !== 'print') {
							fatal("Unknown output type: `%s'", $val);
						}
						$options['output'] = $val;
						break;
					case 'p':
					case 'plan':
						if (!Plans::exists($val)) {
							fatal("Unknown plan `%s'", $val);
						}
						$options['plan'] = $val;
						break;
					case 'backup':
						$options['backup'] = true;
						break;
					case 'c':
						$parsed = self::parseServiceConfiguration($val);
						if (null === $parsed) {
							fatal('Failed to parse %s', $val);
						}
						$options = array_merge_recursive(
							$options, [
							'conf' => $parsed
						]);
						break;
					case 'since':
						if (!\is_int($val)) {
							try {
								$val = (new \DateTime($val))->getTimestamp();
							} catch (\Exception $e) {
								fatal('Unknown/invalid time spec: %s', $val);
							}
						}
						$options['since'] = $val;
						if ($val > time()) {
							fatal("Time assertion failed. Parsed time `%d' is newer than current time `%d'", $val, time());
						}
						break;
					case 'match':
						$options['match'] = $val;
						break;
					case 'reason':
						$options['reason'] = $val;
						break;
					case 'template':
						$options['template'] = $val;
						break;
					case 'dry-run':
					case 'n':
						$options['dry'] = true;
						break;
					case 'reset':
						$options['reset-plan'] = true;
						break;
					case 'notify':
						$options['notify'] = true;
						break;
					case 'bootstrap':
						$options['bootstrap'] = \Util_Conf::inferType($val ?: "true");
						break;
					case '--':
						break 2;
				}

			}
			$command = \array_slice($_SERVER['argv'], $optind);
			if (\in_array('-c', $command, true)) {
				fatal('additional site formatting args (-c) found after site specifier - site specifier MUST come last');
			}

			if (array_get($options, 'output') === 'json') {
				$fd = $options['fd'] ?? Output::STDOUT;
				if ($fd === Output::STDOUT) {
					\Error_Reporter::set_verbose(0);
				}
				register_shutdown_function(static function () use ($fd) {
					return register_shutdown_function(static function () use ($fd) {
						$writer = (new Output)->setFormat('json')->setDescriptor($fd);
						if (!$writer->writeable()) {
							debug("Specified channel `%d' is not writable, defaulting to STDERR", $fd);
							$writer->setDescriptor(Output::STDERR);
						}
						$writer->echo(\Error_Reporter::get_buffer());
						exit((int)\Error_Reporter::is_error());
					});
				});
			}

			return [
				'options' => $options,
				'command' => $command
			];
		}

		/**
		 * Display help
		 *
		 * @return void
		 */
		public static function help(): void
		{
			$flatten = static function ($var) {
				if (\is_array($var)) {
					return '[' . implode(',', $var) . ']';
				}

				return $var;
			};
			echo \Opcenter::version(), PHP_EOL;
			$help = static::getHelpFromModules();
			ksort($help);
			foreach ($help as $module => $vars) {
				printf("%s module\n", $module);
				printf("%s\n", str_repeat('=', 80));
				foreach ($vars as $var => $info) {
					$default = '[default: ' . $flatten($info['default']) . ']';
					printf("    %-12s: %-16s %s %s\n", $var, $default, $flatten($info['range']), $info['help']);
				}
				printf("\n\n");
			}
			echo "Non-module options:\n" .
				str_repeat('=', 80) . "\n" .
				"--since=time             delete suspended sites older than TIME. Mix with dry-run to validate deletability\n" .
				"--backup                 backup metadata before edit\n" .
				"-n, --dry-run            show which sites would be removed/edited without processing\n" .
				"--match=identifier       delete domains that match regular expression IDENTIFIER (delimiter implicitly added)\n" .
				"--reset                  reset plan to configured plan or if -p PLAN specified, target plan\n" .
				"--notify                 notify account holder account created\n" .
				"--bootstrap              issue Let's Encrypt upon creation\n" .
				"--reason=reason          specify a suspension reason\n" .
				"--template=name          use suspension template NAME\n" .
				"-o, --output=[json|print] output normally in json or print each line (stdout)\n" .
				"--fd=descriptor          output to descriptor\n" .
				"-f, --force              force hooks to run if depopulation fails\n\n";

			echo "Sample usage:\n", str_repeat('=', 80), "\n",
			'Create a domain:', PHP_EOL, '    AddDomain -c siteinfo,domain=mydomain.com -c siteinfo,admin_user=myadmin -c auth,passwd=1',
			"\n",
			'Edit a domain:', PHP_EOL, '    EditDomain -c siteinfo,domain=newdomain.com olddomain.com', "\n",
			'Delete a domain:', PHP_EOL, '    DeleteDomain site12', "\n\n";
			echo "Domains may be targeted by domain name (mydomain.com), site (site152), or billing,invoice value (apnscp-FJK21)\n\n";
			exit(1);
		}

		/**
		 * Load help from modules
		 *
		 * @param string      $plan    optional plan to load
		 * @param string|null $service optional service to fetch
		 * @return array
		 */
		public static function getHelpFromModules(string $plan = OPCENTER_DEFAULT_PLAN, string $service = null): array
		{
			$handler = new SiteConfiguration(null); // dummy argument
			$handler->setPlanName($plan);
			$help = [];
			$services = $handler->getDefaultConfiguration($service);
			if ($service) {
				$services = [$service => $services];
			}
			foreach ($services as $svc => $vars) {
				$submodulehelp = [];
				foreach ($vars as $var => $default) {
					$ctx = new ConfigurationContext($svc, $handler);
					$validator = $ctx->getValidatorClass($var);
					/**
					 * @var ServiceValidator $valinstance
					 */
					if ($validator === null) {
						if (SiteConfiguration::STRICT_INTERPRETATION) {
							warn("Unknown entity `%s' in `%s' - ignoring", $var, $svc);
						}
						continue;
					}
					$valinstance = new $validator($ctx, null);
					if ($default === DefaultNullable::NULLABLE_MARKER && $valinstance instanceof DefaultNullable) {
						$default = $valinstance->getDefault();
					}
					$submodulehelp[$var] = [
						'default' => $default,
						'help'    => $validator ? $valinstance->getDescription() : null,
						'range'   => $validator ? $valinstance->getValidatorRange() : null
					];

				}
				$help[$svc] = $submodulehelp;
			}

			if (self::HIDE_RESELLER) {
				unset($help['reseller']);
			}

			return $help;
		}

		/**
		 * Parse command arguments
		 *
		 * @param string|array $sname
		 * @return array
		 */
		public static function parseServiceConfiguration($sname): array
		{
			$parsed = [];
			foreach ((array)$sname as $input) {
				[$svc, $sname] = explode(',', $input, 2);
				[$sname, $b] = explode('=', $sname, 2);
				$sval = static::parseArgs($b);
				array_set($parsed, "${svc}.${sname}", $sval);
			}

			return $parsed;
		}

		/**
		 * Collapse an array into a single string
		 *
		 * @param mixed $args
		 * @return string
		 */
		public static function collapse($args): string {
			if (\is_array($args)) {
				$str = '';
				foreach ($args as $key => $arg) {
					if (!\is_int($key)) {
						$str .= $key . ':';
					}
					$str .= self::collapse($arg);
					$str .= ',';
				}

				return '[' . rtrim($str, ',') . ']';
			}

			if (\is_bool($args)) {
				return $args ? 'true' : 'false';
			}

			if (null === $args) {
				return 'None';
			}

			return (string)$args;
		}

		/**
		 * Convert configuration into CLI arguments
		 *
		 * @param array $config
		 * @return string
		 */
		public static function commandifyConfiguration(array $config): string
		{
			return implode(' ', array_key_map(static function ($key, $val) {
				return '-c ' . escapeshellarg(Str::replaceFirst('.', ',',
						$key)) . '=' . str_replace('%', '%%', escapeshellarg((string)\Util_Conf::build_ini($val)));
			}, array_dot(array_filter($config, static function ($arr) {
				return !empty($arr);
			}))));
		}

		/**
		 * Parse an argument into its components
		 *
		 * When working with commas as an argument value, escape the parameter
		 * or double-quote, e.g.
		 * siteinfo,tpasswd="'foo,bar,baz'" or siteinfo,tpasswd='foo\,bar\,baz'
		 * and not siteinfo,tpasswd='foo,bar,baz'
		 *
		 * @param string|array $args
		 * @return bool|float|int|mixed|\SplStack|string
		 */
		public static function parseArgs(&$args, int $inlist= 0)
		{
			if (is_scalar($args)) {
				$args = preg_split('//', $args, -1, PREG_SPLIT_NO_EMPTY);
			}
			$cmdargs = [];
			$stack = new \SplStack();
			$key = null;
			$inquotes = false;
			$valueExpected = false;
			for (; false !== ($arg = current($args)); next($args)) {
				if ('' === $arg) {
					$cmdargs[] = '';
					continue;
				}
				if ($inquotes) {
					$stack->push($arg);
					if ($inquotes === $arg && $stack->top() !== '\\') {
						// end quoted
						$inquotes = false;
					}
					continue;
				}
				switch ($arg) {
					case '"':
					case "'":
						$stack->push($arg);
						$inquotes = $arg;
						continue 2;
					case ' ':
						if (!$inquotes && $valueExpected) {
							continue 2;
						} else if (!$inquotes && $inlist) {
							// [foo, bar]
							continue 2;
						}

						break;
					case '[':
						next($args);
						$stack->push(static::parseArgs($args, $inlist+1));
						continue 2;
					case ']':
						if (!$inlist) {
							break;
						}
						$inlist--;
						if ($stack->isEmpty()) {
							return [];
						}
						$merged = static::merge($stack);
						if ($key) {
							$cmdargs[$key] = $merged;
						} else {
							$cmdargs[] = $merged;
						}

						return $cmdargs;
					case '\\':
						$stack->push(next($args));
						continue 2;
					case ':':
						if (!$inlist) {
							break;
						}

						// peek ahead to see what other chars are
						// valid: '[foo:bar,baz:que]', invalid: '[foo:bar:baz]'
						for ($i = key($args) + 1, $n = \count($args); $i < $n; $i++) {
							if ($args[$i] === ',' || ( ($args[$i] === ']' || $args[$i] === '[') && $args[$i-1] !== '\\') ) {
								break;
							}
							if ($args[$i] === ':') {
								while (next($args) !== false) {
									$cur = current($args);
									if ($cur === ']') {
										prev($args);
										break 3;
									}
									$arg .= current($args);
								}
							}
						}

						$key = static::merge($stack);
						if (!\is_int($key)) {
							$key = ltrim((string)$key);
						}

						$stack = new \SplStack();
						$valueExpected = true;
						$cmdargs[$key] = $stack;
						continue 2;
					case ',':
						if (!$inlist) {
							break;
						}
						$merged = static::merge($stack);
						if ($key) {
							$cmdargs[$key] = $merged;
							$valueExpected = false;
							$key = null;
						} else {
							$cmdargs[] = $merged;
						}
						$stack = new \SplStack();
						continue 2;
				}
				$stack->push($arg);
			}
			$stack = static::merge($stack);
			if (!\is_array($stack)) {
				return $stack;
			}
			$cmdargs[] = $stack;

			return array_pop($cmdargs);
		}

		private static function merge($stack)
		{
			if (\is_array($stack)) {
				return $stack;
			}
			$str = '';
			if ($stack->isEmpty()) {
				return $str;
			}
			while (!$stack->isEmpty()) {
				$el = $stack->shift();
				if ($el instanceof \SplStack || \is_array($el)) {
					return static::merge($el);
				}
				$str .= $el;
			}
			// impossible to parse empty string as PHP doesn't recognize "" from CLI
			if (($str[0] === "'" || $str[0] === '"') && $str[-1] === $str[0]) {
				return substr($str, 1, -1);
			} else if ($str === 'null' || $str === 'None') {
				// backwards compatibility, support None or null
				return null;
			} else if (is_numeric($str)) {
				return false === strpos($str, '.') ? (int)$str : (float)$str;
			} else if (($tmp = strtolower($str)) === 'false') {
				return false;
			} else if ($tmp === 'true') {
				return true;
			}

			return $str;
		}

		/**
		 * Retrieve site from supplied arguments
		 *
		 * @param array ...$args
		 * @return array
		 */
		public static function getSiteFromArgs(...$args): array
		{
			$sites = [];
			foreach ($args as $arg) {
				$site = 'site' . \Auth::get_site_id_from_anything($arg);

				if ($site !== 'site') {
					$sites[] = $site;
				} else if (null !== ($site = \Auth::get_site_id_from_invoice($arg))) {
					$sites = array_merge($sites, array_map(static function ($v) {
						return 'site' . (string)$v;
					}, $site));
				} else {
					error("unknown site/domain/identifier `%s'", $arg);
				}
			}

			return $sites;
		}

		/**
		 * Convert scalar arguments into flags
		 *
		 * NB: setting a flag to null unsets it
		 *
		 * @param array $flags
		 * @return array
		 */
		public static function buildFlags(array $flags): string {
			$arg = [];
			foreach ($flags as $k => $v) {
				if ($v === false) {
					warn("Useless usage of `%s', ignoring", $k);
					continue;
				} else if ($v === null) {
					continue;
				}

				$arg[] = (\strlen($k) > 1 ? '--' : '-') . escapeshellarg($k) .
					($v !== true ? ('=' . escapeshellarg((string)$v)) : '');
			}

			return implode(' ', $arg);
		}

	}

