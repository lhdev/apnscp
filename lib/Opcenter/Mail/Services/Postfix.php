<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Mail\Contracts\ServiceDaemon;
	use Opcenter\System\GenericSystemdService;

	class Postfix extends GenericSystemdService implements ServiceDaemon
	{
		protected const SERVICE = 'postfix';
		protected const HOME = '/etc/postfix';

		/**
		 * Get path to mail aliases
		 *
		 * @return string
		 */
		public static function getAliasesPath(): string
		{
			return '/etc/aliases';
		}

		/**
		 * Get directory for Postfix overrides
		 *
		 * @return string
		 */
		public static function getMasterOverridePath(): string
		{
			return self::HOME . '/master.d';
		}

		/**
		 * Get overrides for site
		 *
		 * @param string $site
		 * @return array
		 */
		public static function siteOverrides(string $site): array
		{
			$pattern = Postfix::getMasterOverridePath() . '/' . $site . '{-*,}.*';

			return glob($pattern, GLOB_NOSORT | GLOB_BRACE);
		}
	}
