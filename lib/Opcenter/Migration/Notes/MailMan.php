<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Migration\Notes;

use Opcenter\Migration\Note;
use Opcenter\Net\Ip4;
use Opcenter\Net\Ip6;

class MailMan extends Note {
	protected $lookup = [];

	public function setLists(array $lists): self
	{
		$this->lookup = array_flip($lists);
		return $this;
	}

	public function in(string $address): bool
	{
		return \array_key_exists($address, $this->lookup);
	}

	public function get(): array {
		return array_keys($this->lookup);
	}
}