<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Account;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Opcenter\Filesystem;
	use Opcenter\Http\Apache;
	use Opcenter\Map;
	use Opcenter\Provisioning\Siteinfo;
	use Opcenter\Role\Group;
	use Opcenter\Role\User;
	use Opcenter\Service\ConfigurationContext;
	use Opcenter\Service\Contracts\AlwaysRun;
	use Opcenter\Service\ServiceLayer;
	use Opcenter\SiteConfiguration;

	class Delete extends DomainOperation implements Publisher
	{
		const HOOK_ID = 'vddelete';

		use \FilesystemPathTrait;

		// @var array options passed at runtime
		protected $runtimeOptions;
		// @var string domain name
		protected $domain;
		// @var int site id
		protected $site_id;
		// @var string "site" + id
		protected $site;
		/**
		 * @var array
		 */
		protected $savedBuffer;

		/**
		 *
		 * @param string $site
		 */
		public function __construct(string $site, $runtime = [])
		{
			// block dirty global states
			$this->savedBuffer = \Error_Reporter::flush_buffer();
			// avoid firing callbacks on previously registered iterations
			Cardinal::purge();
			$id = (int)substr($site, 4);
			if (!$site || !($this->domain = \Auth::get_domain_from_site_id($id))) {
				$this->site = $site;
				if (0 === strncmp($site, 'site', 4) && file_exists($this->domain_info_path()) &&
					Filesystem::delete($site)) {
					warn("removed orphaned folder structure from `%s'",
						\dirname($this->domain_info_path())
					);
				}
				$this->attemptCleanup($site);
				fatal("Unknown site `%s'", $site);
			}
			$this->site_id = $id;
			$this->site = $site;
			$this->runtimeOptions = $runtime;
		}

		public function __destruct()
		{
			if (\Error_Reporter::is_error() && is_debug() && \Error_Reporter::is_verbose(9999)) {
				pause('Failed to delete %s', $this->site);
			}

			\Error_Reporter::set_buffer($this->savedBuffer);
		}

		private function attemptCleanup(string $site): bool
		{
			$maps = [Map::DOMAIN_TXT_MAP, Map::DOMAIN_MAP];
			foreach ($maps as $mapFile) {
				$map = Map::load($mapFile, 'w');
				$domains = $map->fetchAll();
				array_filter($domains, static function ($value, $key) use ($site, $map) {
					if ($value === $site) {
						$map->delete($key);
					}
				}, ARRAY_FILTER_USE_BOTH);
			}

			return true;
		}

		/**
		 * Attempt to generate the least amount of changes to enter delete context
		 */
		public function recoverCorruptedUser(): void
		{
	        $svc = new SiteConfiguration($this->site);
			(new ServiceLayer($this->site))->mount();
			if (empty($admin = $svc->getServiceValue('siteinfo', 'admin'))) {
				fatal("Cannot regenerate from metadata");
			}

			if (!posix_getpwnam($admin)) {
				Siteinfo::createSysAdmin($admin, $svc->getAccountRoot());
			}

			Siteinfo::populateFilesystem($svc, 'siteinfo');
			serial(static function () use ($admin, $svc) {
				// cover existent group
				Siteinfo::createAdmin(
					$admin,
					$svc->getServiceValue('siteinfo', 'admin_user'),
					$svc->getAccountRoot(),
					'/home/' . $svc->getServiceValue('siteinfo', 'admin_user')
				);
			});
		}

		/**
		 * Recover a corrupted group
		 *
		 * @param SiteConfiguration $svc
		 */
		private function recoverCorruptedGroup(SiteConfiguration $svc): void
		{
			if (posix_getgrnam($group = $svc->getServiceValue('siteinfo', 'admin'))) {
				return;
			}
			$pwd = User::bindTo($svc->getAccountRoot())->getpwnam($svc->getServiceValue('siteinfo',
				'admin_user'));
			if (!$pwd['gid']) {
				fatal("Cannot locate gid for %(site)s admin %(admin_user)s", [
					'site'       => $this->site,
					'admin_user' => $svc->getServiceValue('siteinfo', 'admin_user')
				]);
			}
			warn("Group %s missing - recreating", $group);
			Group::bindTo('/')->create($group, [
				'gid'  => $pwd['gid'],
				'home' => $svc->getAccountRoot()
			]);
		}

		public function processHooks() {
			try {
				\Util_Account_Hooks::instantiateContexted(\Auth::context(null, $this->site))->run('delete');
			} catch (\Error $e) {
				if (!array_get($this->runtimeOptions, 'force')) {
					fatal('failed to delete domain, delete hooks failed - force but be aware of side effects: %s',
						$e->getMessage());
				}
			}

		}

		/**
		 * Run account creation
		 */
		public function exec()
		{
			$handler = new SiteConfiguration($this->site, [], $this->runtimeOptions);

			if ((new ConfigurationContext('siteinfo', $handler))->isEdit()) {
				if (empty($this->runtimeOptions['force'])) {
					fatal('Unable to delete site %d - finish EditDomain first', $this->site_id);
				}
				Filesystem::rmdir($this->domain_info_path('new'));
				Filesystem::mkdir($this->domain_info_path('new'));
				$handler = new SiteConfiguration($this->site);

			}

			// prevent deleting a site while configuration reloads
			if (\Opcenter\Http\Apache::cancelRebuild()) {
				defer($_, static function() {
					Apache::activate();
				});
			}
			// ensure we cleanup journaled data that completed
			register_shutdown_function(function () use ($handler) {
				$this->fireCleanup($handler);
			});

			// recovery check for corrupted group
			$this->recoverCorruptedGroup($handler);

			$this->runUserHooks($handler);

			foreach (array_reverse($handler->getServices()) as $svc) {
				$ctx = new ConfigurationContext($svc, $handler);

				// reverse service values to ensure "version" then "enabled" are final to trigger
				$vars = array_reverse(array_keys($ctx->toArray()));
				foreach ($vars as $var) {
					if (null === ($checker = $ctx->getValidatorClass($var))) {
						// nothing to do to rollback
						continue;
					}

					/**
					 * @var $cls \Opcenter\Service\ServiceValidator
					 */
					$cls = new $checker($ctx, $this->site);
					if (!$cls instanceof AlwaysRun && !$ctx->getServiceValue($svc, 'enabled')) {
						continue;
					}
					if ($cls instanceof \Opcenter\Service\Contracts\ServiceInstall && !$cls->depopulate($handler)) {
						return error("failed to depopulate service `%s' on svc var `%s'", $svc, $var);
					}
				}
			}
			$this->fireCleanup($handler);
			\apnscpSession::invalidate_by_site_id($this->site_id);
			if (!Filesystem::delete($this->site)) {
				return false;
			}
			$this->status = self::RC_SUCCESS;

			return true;
		}



		public function getDomain()
		{
			return $this->domain;
		}

		public function getEventArgs()
		{
			return [
				'site'    => $this->site,
				'domain'  => $this->domain,
				'options' => $this->runtimeOptions
			];
		}
	}