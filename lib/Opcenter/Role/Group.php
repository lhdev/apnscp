<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Role;

	use Opcenter\Contracts\Virtualizable;

	class Group implements Virtualizable
	{
		const GROUP_FILE = '/etc/group';
		const MIN_GID = 500;

		protected $root;

		public function __construct(string $root = '/')
		{
			$this->root = $root;
		}

		public static function bindTo(string $root = '/')
		{
			return new static($root);
		}

		/**
		 * Create new group
		 *
		 * @param string $grname
		 * @param array  $options gid N,
		 * @return bool
		 */
		public function create(string $grname, array $options = []): bool
		{
			if (!preg_match(\Regex::GROUPNAME, $grname)) {
				return error("invalid group name `%s'", $grname);
			}
			$attr2flag = [
				'gid'       => '-g',
				'duplicate' => '-o',
				'system'    => '-r',
				'force'     => '-f'
			];

			if (false === ($flags = $this->parseFlags($attr2flag, $options))) {
				return false;
			}
			$ret = (new \Util_Process_Chroot($this->root))
				->run('/usr/sbin/groupadd %s %s', $flags, $grname);
			if (!$ret['success']) {
				return error($ret['stderr']);
			}

			return $ret['success'];
		}

		/**
		 * Convert options into driver parameters
		 *
		 * @param array $attributes
		 * @param array $options
		 * @return bool|null|string
		 */
		protected function parseFlags(array $attributes, array $options)
		{
			$flags = '';
			foreach ($options as $opt => $val) {
				if (!isset($attributes[$opt])) {
					warn("option name `%s' unrecognized", $opt);
					continue;
				}
				switch ($opt) {
					case 'gid':
						if (!\is_int($val) || $val < 0) {
							return error("gid must be integer > 0, `%s' given", $val);
						}
						break;
				}
				if ($val === false) {
					// whhhhhhhhhyyyyyyyyyyyyyyyy!!!
					continue;
				}
				$flags .= ' ' . $attributes[$opt];
				if ($val !== null && $val !== true) {
					$flags .= escapeshellarg((string)$val);
				}
			}

			return $flags ?: null;
		}

		/**
		 * Add a member to a group
		 *
		 * @param string $user
		 * @param string $grname
		 * @return bool
		 */
		public function addMember(string $user, string $grname): bool
		{
			if (!preg_match(\Regex::USERNAME, $user)) {
				return error("invalid user `%s'", $user);
			}
			if (!$this->exists($grname)) {
				return error("invalid group `%s'", $grname);
			}
			if (\in_array($user, $this->members($grname), true)) {
				return warn("user `%s' is already part of group `%s'", $user, $grname);
			}
			$ret = (new \Util_Process_Chroot($this->root))->run('/usr/sbin/groupmems -a %s -g %s', $user, $grname);
			if (!$ret['success']) {
				return error($ret['stderr']);
			}

			return true;
		}

		/**
		 * Verify group exists
		 *
		 * @param string $grname
		 * @return bool
		 */
		public function exists(string $grname): bool
		{
			return $this->getgrnam($grname) !== null;
		}

		/**
		 * Virtualized getgrnam()
		 *
		 * @param null|string $grname dump group on null
		 * @return null|array
		 */
		public function getgrnam(?string $grname): ?array
		{
			if ($grname && !preg_match(\Regex::GROUPNAME, $grname)) {
				error("invalid group name `%s'", $grname);

				return null;
			}
			$pwd = array();
			$file = rtrim($this->root, '/') . self::GROUP_FILE;
			$fp = fopen($file, 'r');
			if (!$fp) {
				error('unable to open %s', self::GROUP_FILE);

				return null;
			}
			while (false !== ($line = fgets($fp))) {
				$line = explode(':', trim($line));

				if (\count($line) < 4) {
					continue;
				}
				$mygroup = $line[0];

				$pwd[$mygroup] = array(
					'passwd'  => $line[1],
					'gid'     => (int)$line[2],
					'members' => explode(',', $line[3])
				);
			}
			fclose($fp);
			if (!$grname) {
				return $pwd;
			}

			return $pwd[$grname] ?? null;
		}

		/**
		 * Get members of group
		 *
		 * @param string $grname
		 * @return array|null
		 */
		public function members(string $grname): ?array
		{
			if (!preg_match(\Regex::GROUPNAME, $grname)) {
				error("invalid group name `%s'", $grname);

				return null;
			}
			$ret = (new \Util_Process_Chroot($this->root))->run('/usr/sbin/groupmems -l -g %s', $grname);
			if (!$ret['success']) {
				error($ret['stderr']);

				return null;
			}

			return (array)preg_split('/\s+/', $ret['output'], -1, PREG_SPLIT_NO_EMPTY);

		}

		/**
		 * Remove a member from a group
		 *
		 * @param string $user
		 * @param string $grname
		 * @return bool
		 */
		public function removeMember(string $user, string $grname): bool
		{
			if (!preg_match(\Regex::USERNAME, $user)) {
				return error("invalid user `%s'", $user);
			}
			if (!$this->exists($grname)) {
				return error("invalid group `%s'", $grname);
			}
			if (!\in_array($user, $this->members($grname), true)) {
				return warn("user `%s' not part of group `%s'", $user, $grname);
			}
			$ret = (new \Util_Process_Chroot($this->root))->run('/usr/sbin/groupmems -d %s -g %s', $user, $grname);
			if (!$ret['success']) {
				return error($ret['stderr']);
			}

			return true;
		}

		/**
		 * Delete group
		 *
		 * @param string $grname
		 * @return bool
		 */
		public function delete(string $grname): bool
		{
			if (!preg_match(\Regex::GROUPNAME, $grname)) {
				return error("invalid group name `%s'", $grname);
			}
			if (!$this->exists($grname)) {
				return error("group `%s' does not exist", $grname);
			}
			$proc = new \Util_Process_Chroot($this->root);
			$ret = $proc->run('/usr/sbin/groupdel %s', $grname);
			if (!$ret['success']) {
				return error($ret['stderr']);
			}

			return $ret['success'];
		}

		/**
		 * Modify existing group
		 *
		 * @param string $grname
		 * @param array  $attributes
		 * @return bool
		 */
		public function change(string $grname, array $attributes): bool
		{
			if (!preg_match(\Regex::GROUPNAME, $grname)) {
				return error("invalid group name `%s'", $grname);
			}
			if (!$this->exists($grname)) {
				return error("group `%s' does not exist", $grname);
			}
			$attr2flag = [
				'gid'       => '-g',
				'name'      => '-n',
				'duplicate' => '-o'
			];
			if (false === ($flags = $this->parseFlags($attr2flag, $attributes))) {
				return false;
			}
			$proc = new \Util_Process_Chroot($this->root);
			$ret = $proc->run('/usr/sbin/groupmod %s %s', $flags, $grname);
			if (!$ret['success']) {
				return error($ret['stderr']);
			}

			return $ret['success'];
		}
	}