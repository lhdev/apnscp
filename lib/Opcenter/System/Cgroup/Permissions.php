<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup;

	use Opcenter\System\Cgroup;

	class Permissions implements Cgroup\Contracts\Renderable
	{
		public const DEFAULTS = [
			// task uid
			'task.uid'    => 'root',
			// task gid
			'task.gid'    => 'root',
			// task permissions
			'task.fperm'  => 0664,
			// non-task file ownership
			'admin.uid'   => 'root',
			// non-task group ownership
			'admin.gid'   => 'root',
			// group directory permission
			'admin.dperm' => 0755,
			// file permission mode
			'admin.fperm' => 0640,
		];
		private static $permissionSymbols =
			[
				'task.uid'    => 'tuid',
				'task.gid'    => 'tgid',
				'task.fperm'  => 'tfperm',
				'admin.uid'   => 'auid',
				'admin.gid'   => 'agid',
				'admin.dperm' => 'adperm',
				'admin.fperm' => 'afperm',
			];
		protected $permissions = self::DEFAULTS;

		public function __construct(array $permissions = [])
		{
			if (!$permissions) {
				return;
			}
			$permissions = array_dot($permissions);
			$bad = array_diff_key($permissions,
				array_filter($permissions, 'self::permissionValid', ARRAY_FILTER_USE_BOTH));
			if ($bad) {
				fatal("Unknown cgroup permission attributes specified: `%s'", implode(', ', array_keys($bad)));
			}

			$this->permissions = array_replace(static::DEFAULTS, $permissions);
		}

		/**
		 * Set permission
		 *
		 * @param string $name
		 * @param        $value
		 * @return bool
		 */
		public function setPermission(string $name, $value): bool
		{
			if (!self::permissionValid($name)) {
				fatal("Unknown cgroup permission attribute specified: `%s'", $name);
			}
			$this->permissions[$name] = $value;

			return true;
		}

		/**
		 * cgroup controller permission valid
		 *
		 * @param string $permission
		 * @param mixed  $value
		 * @return bool
		 */
		public static function permissionValid($value, string $permission): bool
		{
			if (($permission === 'task.uid' || $permission === 'task.gid' || $permission === 'admin.uid' || $permission === 'admin.gid') && \is_int($value)) {
				fatal("cgconfig only accepts user/group names - `%s' must be given as name not id", $permission);
			}

			return isset(static::DEFAULTS[$permission]);
		}

		public function setPermissions(array $p): bool
		{
			if ($bad = array_diff_key(static::DEFAULTS, $p)) {
				return error('Unknown cgroup permission variables: %s', implode(',', $bad));
			}
			$this->permissions = array_merge($this->permissions, $p);

			return true;
		}

		/**
		 * Get permission value
		 *
		 * @param string $name
		 * @return mixed|null
		 */
		public function getPermission(string $name)
		{
			return $this->permissions[$name] ?? null;
		}

		public function get(): array
		{
			return array_merge(static::DEFAULTS, $this->permissions);
		}

		public function __toString(): string
		{
			return $this->build();
		}

		public function build(): string
		{
			if (!$this->permissions) {
				return '';
			}
			$map = [];
			foreach ($this->permissions as $dot => $val) {
				array_set($map, $dot, $val);
			}
			$configs = [];

			foreach ($map as $mode => $perms) {
				$configs[] = $mode . ' { ' .
					implode(' ', array_key_map(static function ($k, $v) {
						if (\is_int($v)) {
							$v = decoct($v);
						}

						return "$k = $v;";
					}, $perms)) .
					' }';
			}

			return 'perm { ' . implode(' ', $configs) . ' }';
		}
	}