<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup\Attributes\Memory;

	use Opcenter\System\Cgroup\Attributes\BaseAttribute;
	use Opcenter\System\Cgroup\Controller;

	class LimitInBytes extends BaseAttribute
	{
		public function __construct($value, Controller $controller)
		{
			parent::__construct($value ?? -1, $controller);
			if (-1 !== $this->value) {
				$this->value *= 1024 ** 2;
			}
		}

		public function deactivate(): bool
		{
			$path = $this->getAttributePath();

			return file_put_contents($path, -1) > 0;
		}
	}