<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	/**
	 * Trait PropertyAccessorTrait
	 * Automatically expand properties, utilize getter/setter when present
	 */
	trait PropertyMutatorAccessorTrait
	{

		public function __isset($name)
		{
			return property_exists($this, $name) && $this->{$name} !== null;
		}

		/**
		 * Get property with optional accessor
		 *
		 * @param $name
		 * @return mixed
		 */
		public function __get($name)
		{
			if (!property_exists($this, $name)) {
				fatal("Unknown property `%s'", $name);
			}

			$helper = 'get' . camel_case($name);
			if (method_exists($this, $helper)) {
				return $this->{$helper}();
			}

			return $this->{$name};
		}

		/**
		 * Set property with optional type check
		 *
		 * Type check will cause a fatal error if setName returns false
		 *
		 * @param $name
		 * @param $value
		 */
		public function __set($name, $value)
		{
			if (!property_exists($this, $name)) {
				fatal("Unknown property `%s'", $name);
			}

			$helper = 'set' . camel_case($name);
			if (method_exists($this, $helper)) {
				$ret = $this->{$helper}($value);
				if ($ret === false) {
					fatal('Failed to set %s property, failed type check', $name);
				} else if ($ret === null) {
					return;
				}
			}
			$this->{$name} = $value;
		}
	}