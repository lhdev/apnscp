<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	define('enum', 0x0001);
	define('bool', 0x0002);
	define('string', 0x0004);
	define('set', 0x0008);
	define('double', 0x0010);
	define('int', 0x0020);
	define('integer', 0x0020);
	define('enum_radio', 0x0040);
	define('text', 0x0080);

	define('OPT_REQUIRED', 0x0001);
?>
