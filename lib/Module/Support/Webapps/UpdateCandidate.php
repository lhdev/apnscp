<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Module\Support\Webapps;

	use Lararia\Jobs\Job;
	use Lararia\Jobs\Traits\RunAs;
	use Module\Support\Webapps\MetaManager\Meta;
	use Opcenter\Versioning;

	class UpdateCandidate
	{
		use \apnscpFunctionInterceptorTrait, RunAs {
			getAuthContext as public;
		}

		const NOTIFY_UPDATE_PREFERENCE = 'webapps.notify';

		const BLOCKED_UPDATES = [
			'joomla',
			'magento'
		];

		protected static $fieldMaps = [
			'type'            => 'setType',
			'version'         => 'setVersion',
			'options.verlock' => 'setLockVersion',
			'options.git'     => 'enableAssuranceMode'
		];
		/**
		 * @var string hostname
		 */
		protected $hostname;
		/**
		 * @var string optional path component
		 */
		protected $path = '';
		/**
		 * @var string application type
		 */
		protected $appType;
		/**
		 * @var ?string version lock type, 'major', 'minor', or null
		 */
		protected $lockType;
		/**
		 * @var string detected app version
		 */
		protected $version;
		/**
		 * @var string|null next version
		 */
		protected $nextVersion;
		/**
		 * @var array available version candidates
		 */
		protected $versions;
		/**
		 * @var \Auth_Info_User
		 */
		protected $context;

		protected $hasAssurance = false;
		/**
		 * @var Assurance
		 */
		protected $assurance;

		/**
		 * UpdateCandidate constructor.
		 *
		 * @param string $hostname hostname
		 * @param string $path     path
		 */
		public function __construct(string $hostname, string $path = '')
		{
			$this->hostname = $hostname;
			$this->path = $path;
			$this->__wakeup();
		}

		/**
		 * Parse information from app info
		 *
		 * @param array|Meta $appinfo
		 * @return bool
		 */
		public function parseAppInformation($appinfo): bool
		{
			$changed = 0;
			foreach (self::$fieldMaps as $field => $method) {
				$val = array_get($appinfo, $field, null);
				if (null === $val) {
					continue;
				}
				if (!$this->$method($val)) {
					warn("failed to autopopulate field `%s' using value `%s'", $field, $val);
					continue;
				}
				$changed++;
			}

			return $changed > 0;
		}

		/**
		 * Candidate is version-locked
		 *
		 * @return bool
		 */
		public function isLocked(): bool
		{
			$idx = \count($this->versions) - 1;

			return !$this->getNextVersion() && $this->getVersion() !== $this->versions[$idx];
		}

		/**
		 * Calculate and return next upgrade version
		 *
		 * @return null|string
		 */
		public function getNextVersion(): ?string
		{
			if ($this->nextVersion) {
				return $this->nextVersion;
			}
			if (empty($this->versions)) {
				warn('no version candidates found, skipping upgrade');

				return null;
			}
			$maximal = '999999999.9999999999999.99999999999999';
			if ($this->lockType) {
				if ($this->lockType === 'major') {
					$maximal = Versioning::asMajor($this->version);
				} else {
					$maximal = Versioning::asMinor($this->version);
				}
			}

			$this->nextVersion = Versioning::nextVersion($this->versions, $this->version, $maximal);
			if ($this->nextVersion === $this->version) {
				/**
				 * Already at max version
				 */
				$this->nextVersion = false;
			}

			return $this->nextVersion ?: null;

		}

		/**
		 * Force an update to specified version
		 *
		 * @param string $version
		 * @return $this
		 */
		public function forceUpdateVersion(string $version): self
		{
			$this->nextVersion = $version;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getVersion(): string
		{
			return $this->version;
		}

		/**
		 * Set candidate version
		 *
		 * @param string $version
		 * @return bool
		 */
		public function setVersion(string $version): bool
		{
			$this->version = $version;
			$this->nextVersion = null;

			return true;
		}

		/**
		 * Expose available upgrade versions
		 *
		 * @param array $versions
		 * @return bool
		 */
		public function setAvailableVersions(array $versions): bool
		{
			$this->versions = $versions;

			return true;
		}

		public function logJob(string $baseversion)
		{
			info('%s %s batch: new upgrade task - %s (%s) %s -> %s',
				Job::ICON_INFO,
				$this->getSite(),
				$this->getBaseUri(),
				$this->getAppType(),
				$baseversion,
				$this->getVersion()
			);
		}

		public function getSite(): string
		{
			return $this->getAuthContext()->site;
		}

		/**
		 * Get qualified URI for webapp installation
		 *
		 * @return string
		 */
		public function getBaseUri(): string
		{
			$host = $this->getHostname();
			if (false === strpos($host, '.')) {
				$host .= '.' . $this->getAuthContext()->domain;
			}

			return rtrim($host . '/' . ltrim($this->getPath(), '/'), '/');
		}

		/**
		 * Get hostname
		 *
		 * @return string
		 */
		public function getHostname(): string
		{
			return $this->hostname;
		}

		/**
		 * Get candidate path
		 *
		 * @return string
		 */
		public function getPath(): string
		{
			return $this->path;
		}

		/**
		 * @return string
		 */
		public function getAppType(): string
		{
			return (string)$this->appType;
		}

		/**
		 * @return mixed
		 */
		public function getLockType()
		{
			return $this->lockType;
		}

		/**
		 * Candidate capable of being updated
		 *
		 * @return bool
		 */
		public function isUpdatable(): bool
		{
			if (\in_array($this->getAppType(), self::BLOCKED_UPDATES, true)) {
				// @todo joomla 1-click updates
				return false;
			}

			return (bool)$this->nextVersion;
		}

		public function setType(string $type): bool
		{
			$this->appType = $type;
			$verType = $this->getClassFromType($type);
			$this->setLockVersion($verType::DEFAULT_VERSION_LOCK);

			return true;
		}

		protected function getClassFromType(string $type)
		{
			return \apnscpFunctionInterceptor::get_autoload_class_from_module($type);
		}

		/**
		 * Set upgrade version lock
		 *
		 * @param string|bool $mode major, minor, or null
		 * @return bool
		 */
		public function setLockVersion($mode = ''): bool
		{
			if ($mode === 'none' || !$mode) {
				$mode = false;
			} else if ($mode !== 'major' && $mode !== 'minor' && $mode) {
				return error("unknown version lock mode `%s'", $mode);
			}
			$this->lockType = $mode;

			return true;
		}

		/**
		 * Candidate has snapshot support
		 *
		 * @param bool|int|string $mode
		 */
		public function enableAssuranceMode($mode): bool
		{
			$this->hasAssurance = (bool)$mode;
			return true;
		}

		/**
		 * Run update
		 *
		 * @return bool
		 */
		public function process(): bool
		{
			if (!$this->getNextVersion()) {
				return $this->isLocked() ? warn('version upgrades exist but candidate is locked preventing further upgrades') :
					error('unable to process upgrade, next version could not be calculated');
			}
			if (!$this->appType) {
				return error('no app type specified - call parseAppInformation() first');
			}
			debug('Upgrade command: %s %s %s %s',
				$this->getUpgradeCommand(),
				$this->getHostname(),
				$this->getPath(),
				$this->getNextVersion()
			);

			return $this->apnscpFunctionInterceptor->call(
				$this->getUpgradeCommand(),
				[$this->getHostname(), $this->getPath(), $this->getNextVersion()]
			);
		}

		/**
		 * Bind assurance to update
		 *
		 * @param Git $git
		 * @return Assurance|null
		 */
		public function initializeAssurance(Git $git): ?Assurance
		{
			if (!$this->hasAssurance) {
				return null;
			}

			if ($this->assurance) {
				warn('Overwriting assurance handler - this is most certainly a bug');
			}

			return $this->assurance = Assurance::instantiateContexted($this->getAuthContext(), [$this, $git]);
		}

		/**
		 * Get assurance bond
		 *
		 * @return Assurance|null
		 */
		public function getAssurance(): ?Assurance
		{
			return $this->assurance;
		}

		public function validateAssurance(): bool
		{
			if (!$this->hasAssurance) {
				return true;
			}

			if ($this->assurance->assert()) {
				return true;
			}

			$this->assurance->rollback();
			return false;
		}

		public function getUpgradeCommand()
		{
			return $this->appType . '_update_all';
		}

		/**
		 * Job will retry
		 *
		 * @return bool
		 */
		public function willRetry(): bool {
			return static::class !== self::class; // asset
		}
	}