<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum;

	class SkipList
	{
		const SKIP_LIST = 'synchronizer.skiplist';
		const SKIP_LIST_CACHE = 'cache/synchronizer.skiplist.cache';

		private static $_instance;
		private $_classList = array();

		public function __construct()
		{
			$this->compileSkipList();
			$file = $this->getSkipListCachePath();
			if (file_exists($file)) {
				$this->_classList = unserialize(file_get_contents($file));
			}

		}

		/**
		 * Recompile skip list
		 *
		 * @return bool
		 */
		protected function compileSkipList()
		{
			$skipfile = $this->getSkipListPath();
			$cachefile = $this->getSkipListCachePath();
			if (!file_exists($skipfile)) {
				return true;
			} else if (file_exists($cachefile) && filemtime($cachefile) >= filemtime($skipfile)) {
				return true;
			}
			$files = file($skipfile, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);

			return file_put_contents($cachefile, serialize($files));
		}

		protected function getSkipListPath(): string
		{
			return conf_path(self::SKIP_LIST);
		}

		protected function getSkipListCachePath(): string
		{
			return storage_path(self::SKIP_LIST_CACHE);
		}

		public static function in($file)
		{
			if (null === self::$_instance) {
				$c = __CLASS__;
				self::$_instance = new $c;
			}

			$list = self::$_instance->getList();
			for ($i = 0, $n = \count($list); $i < $n; $i++) {
				if (fnmatch($list[$i], $file)) {
					return true;
				}
			}

			return false;
		}

		public function getList()
		{
			return $this->_classList;
		}
	}