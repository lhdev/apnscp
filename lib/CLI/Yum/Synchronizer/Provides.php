<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;

	/**
	 * Class CLI_Yum_Synchronizer_Generate
	 *
	 * Generate an index of RPM files
	 */
	class Provides extends Synchronizer
	{
		// @var static
		private static $_instance;
		private $_cache = array();

		public function __construct($package = '')
		{
			$this->flush();
			$this->setPackage($package);
		}

		/**
		 * Empty in-memory RPM cache
		 */
		public function flush(): void
		{
			$file = storage_path(SynchronizerCache::CACHE);
			if (!file_exists($file)) {
				warn("missing RPM cache, running `generate' to build yum cache");
				(new Generate())->run();
			}
		}

		/**
		 * Set active package
		 *
		 * @param $package
		 */
		protected function setPackage($package)
		{
			$this->package = $package;
		}

		/**
		 * Static wrapper to lookup
		 *
		 * @param $package
		 * @return array
		 */
		public static function fetch($package)
		{
			if (self::$_instance === null) {
				self::$_instance = new static;
			}
			$meta = Utils::getMetaFromPackage($package);

			return self::$_instance->lookup($package, $meta['version'] ?? null, $meta['release'] ?? null);
		}

		/**
		 * Get installed package from file
		 *
		 * @param string $file
		 * @return null|string
		 */
		public function lookupFile(string $file): ?string
		{
			foreach (SynchronizerCache::get() as $packageName => $package) {
				if ($package->has($file)) {
					return $package->name();
				}
			}

			return null;
		}

		/**
		 * Synchronizer main method
		 *
		 * @return array
		 */
		public function run()
		{
			return $this->lookup($this->package);
		}

		/**
		 * Get RPM file listing
		 *
		 * @param string $package package name
		 * @param null   $version
		 * @param null   $release
		 * @return array
		 */
		public function lookup($package, $version = null, $release = null): array
		{
			if (null === ($obj = SynchronizerCache::get()->getPackage($package, $version, $release))) {
				return [];
			}

			return $obj->files();
		}
	}