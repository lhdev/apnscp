<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;

	/**
	 * Class CLI_Yum_Synchronizer_Update
	 *
	 * Remove obsoleted package and install superior package
	 */
	class Obsoleting extends Synchronizer
	{

		public function run()
		{
			$package = $this->package;
			/**
			 * first remove obsoleted packages, if installed, on account
			 * if packages not installed in FST, skip updating
			 *
			 * NB: "obsoleted" removes packages, "obsoleting" supercedes
			 *
			 */

			$remove = $this->getObsoletesFromPackage($package);

			if (!$remove) {
				// Indicates synchronizer cache is out of date or mischevious forces at play
				report("Missed obsoletes for `%s'", $package);
				return warn("failed to remove obsoletes for `%s'", $package);
			} else if (null === ($svc = $this->removeObsoletes($remove))) {
				// can't locate obsoleted package's old location, ignore
				return true;
			}

			$c = new Install($package, $svc);
			return $c->run();
		}

		/**
		 * Get packages obsoleted by new package
		 *
		 * @param string $package
		 * @return array|false
		 */
		protected function getObsoletesFromPackage($package)
		{
			$ret = \Util_Process_Safe::exec('rpm -q --queryformat="%%{OBSOLETES}" %s', $package);
			if (!$ret['success']) {
				return error("failed to query obsoletes for `%s'", $package);
			}

			return explode(' ', trim($ret['output']));
		}

		/**
		 * Remove obsoleted packages from template
		 *
		 * @param array $pkgs packages to remove
		 * @return null|string last seen service
		 */
		protected function removeObsoletes(array $pkgs)
		{
			$service = null;
			foreach ($pkgs as $pkg) {
				if (!Utils::packageInstalled($pkg)) {
					continue;
				}
				$tmp = $this->getFilesFromCache($pkg);
				// someone didn't generate cache...
				if (!$tmp && !($tmp = Utils::getFilesFromRPM($pkg))) {
					continue;
				}
				$service = Utils::getServiceFromPackage($pkg);
				$c = new Remove($pkg);
				$c->run();

			}

			return $service;
		}

		protected function postUpdate($package, $version, $rel = '')
		{
			$db = \PostgreSQL::initialize();
			$q = "UPDATE site_packages SET version = '" .
				pg_escape_string($version) . "', release = '" . pg_escape_string($rel) .
				"' WHERE package_name = '" . pg_escape_string($package) . "'";
			$db->query($q);
			Synchronizer\Plugins\Manager::run($package, 'update');

			return $db->affected_rows() > 0;
		}
	}