<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;
	use CLI\Yum\Synchronizer\Plugins\Manager;
	use Opcenter\Service\ServiceLayer;

	class Utils
	{
		use \FilesystemPathTrait;

		/**
		 * Get package meta via RPM
		 *
		 * @param $package
		 * @return array|false
		 */
		public static function getMetaFromRPM($package)
		{
			$ret = \Util_Process_Safe::exec(
				'rpm -q --queryformat="%%{VERSION} %%{RELEASE}" %s',
				$package
			);
			if (!$ret['success']) {
				return error('failed to query package %s: %s', $package, $ret['output']);
			}
			list ($version, $release) = explode(' ', trim($ret['output']), 2);
			$meta = array(
				'version' => $version,
				'release' => $release
			);

			return $meta;
		}

		/**
		 * Package installed
		 *
		 * @param string $package
		 * @return bool
		 */
		public static function exists(string $package): bool
		{
			return array_get(\Util_Process_Safe::exec('rpm -q %s', [$package]), 'success', false);
		}

		public static function getServicePath($service): string
		{
			return FILESYSTEM_TEMPLATE . '/' . $service;
		}

		/**
		 * Get version + release from installed package
		 *
		 * @param string $package
		 * @return null|string
		 */
		public static function getFullVersionFromPackage(string $package): ?string
		{
			if (!($meta = self::getMetaFromPackage($package))) {
				return null;
			}

			return $meta['version'] . '-' . $meta['release'];

		}

		/**
		 * Get installed package meta
		 *
		 * @param string $package
		 * @return array meta or empty array if not installed
		 */
		public static function getMetaFromPackage(string $package): array
		{
			$db = \PostgreSQL::pdo();
			$rs = $db->query('SELECT * FROM site_packages WHERE package_name = ' . $db->quote($package));
			if (!$rs || $rs->rowCount() < 1) {
				return [];
			}

			return array_except($rs->fetch(\PDO::FETCH_ASSOC), ['package_name']);
		}

		/**
		 * Determine whether package is installed in template
		 *
		 * @param $package
		 * @return bool
		 */
		public static function packageInstalled($package): bool
		{
			return null !== self::getServiceFromPackage($package);
		}

		/**
		 * Get service under which package is installed
		 *
		 * @param string $package
		 * @return null|string
		 */
		public static function getServiceFromPackage($package)
		{
			return array_get(self::getMetaFromPackage($package), 'service', null);
		}

		/**
		 * Get files in rpm package
		 *
		 * Equivalent to rpm -ql
		 *
		 * @param array $packages
		 * @return array|bool
		 */
		public static function getFilesFromRPM(...$packages)
		{
			$proc = \Util_Process::exec('rpm -q --queryformat "%{NAME} %{ARCH}\n[%{FILENAMES}\n]" ' .
				implode(' ', array_map('escapeshellarg', $packages)));
			if (!$proc['success']) {
				if (\count($packages) === 1) {
					return error("failed to enumerate package `%s'", $packages[0]);
				}
				warn('One more more packages failed to enumerate: %s', $proc['stderr']);
			}

			[$package, $arch] = explode(' ', strtok($proc['output'], "\n"), 2);

			$rpmfilelist = [];
			$rpmlist = [
				$package => $rpmfilelist
			];

			$skip = !\in_array($arch, Synchronizer::ACCEPTABLE_ARCHES, true); // *grumble*
			while (true) {
				$line = strtok("\n");
				if ($line === false || $line[0] !== '/') {
					if ($line !== false) {
						[$line, $arch] = explode(' ', $line, 2);
						// package is not installed, skip
						// [package: package, arch: "fooba is not installed"]
						if (false !== strpos($arch, ' ')) {
							continue;
						}
					}

					if (!$skip) {
						if (Manager::hasPlugin($package, 'filelist')) {
							// @var Plugins\Filelist $plugin
							$plugin = Manager::instantiatePlugin($package, 'filelist');
							$rpmfilelist = $plugin->appendFiles($plugin->filterFiles($rpmfilelist));
						}
						$rpmlist[$package] = $rpmfilelist;
					}

					if ($line === false) {
						break;
					}
					$package = $line;
					$skip = !\in_array($arch, Synchronizer::ACCEPTABLE_ARCHES, true);
					$rpmfilelist = [];
				} else if (!$skip) {
					$rpmfilelist[] = $line;
				}
			}
			return \count($packages) === 1 ? array_pop($rpmlist) : $rpmlist;
		}

		/**
		 * Schedule task to reload filesystem
		 *
		 * @param string $when processing delay
		 * @return bool command scheduled
		 */
		public static function reloadFilesystem(string $when = '2 minutes'): bool
		{
			$id = 'YUM_RELOAD';
			$schedule = new \Util_Process_Schedule($when);
			$schedule->setID($id);
			if ($schedule->preempted($id)) {
				return false;
			}
			$schedule->run(ServiceLayer::MOUNT_CMD . ' reload');
			return true;
		}
	}
