<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Filelist;

	use CLI\Yum\Synchronizer\Plugins\Filelist;

	/**
	 * Class Logrotate
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Filelist
	 *
	 * Override filelist
	 *
	 */
	class Logrotate extends Filelist
	{
		const EXCLUDED_FILES = [
			'/etc/cron.daily/logrotate',
			'/etc/logrotate.conf',
			'/var/lib/logrotate/logrotate.status'
		];
	}