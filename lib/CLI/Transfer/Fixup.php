<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use Module\Support\Auth;

	/**
	 * Adjust account creation options
	 *
	 * @author Matt Saladna <matt@apisnetworks.com>
	 */
	abstract class CLI_Transfer_Fixup
	{
		const MAX_STEP = 0.5;
		protected $_cfg;
		protected $_targetVersion;
		protected $_srcVersion;
		protected $_modifiedVersion;
		protected $logger;

		public static function use_fixup($name): self
		{
			$name = __CLASS__ . '_' . ucwords($name);
			$c = new $name();

			return $c;
		}

		public function setLogger($logger)
		{
			if (!is_callable($logger)) {
				return error('object passed to setLogger is not a valid method');
			}
			$this->logger = $logger;
		}

		public function fix($cfg, array $platform)
		{
			if (!isset($platform['src']) || !isset($platform['dest'])) {
				fatal('missing platform src, dest parameters');
			}
			if (version_compare($platform['src'], $platform['dest'], '!=')) {
				$this->log('different source/dest platform %.1f/%.1f', $platform['src'], $platform['dest']);
			}

			$this->_targetVersion = $dest = $platform['dest'];
			$this->_srcVersion = $src = $platform['src'];
			$this->setPlatformState($src);
			$direction = ($src > $dest) ? -1 : 1;
			$task = ($direction > 0 ? 'upgrading ' : 'downgrading ') . 'from %.1f to %.1f';

			// don't carry over ssh port config
			if (!empty($cfg['ssh']['port_index'])) {
				$this->log('unsetting ssh port index');
				unset($cfg['ssh']['port_index']);
			}
			unset($cfg[Auth_Module::getAuthService()]['pwoverride']);

			while ($this->_targetVersion != $this->_modifiedVersion) {
				dlog($task, $this->getPlatformState(), $this->_targetVersion);
				if ($direction > 0) {
					// upgrade
					$cfg = $this->upgrade($cfg);
					if ($this->getPlatformState() > $this->_targetVersion) {
						fatal('failed platform sanity check, %.1f > %.1f',
							$this->getPlatformState(),
							$this->_targetVersion
						);
					}
				} else {
					// downgrade
					$cfg = $this->downgrade($cfg);
					if ($this->getPlatformState() < $this->_targetVersion) {
						fatal('failed platform sanity check %.1f < %.1f',
							$this->getPlatformState(),
							$this->_targetVersion
						);
					}
				}
			}

			return $cfg;
		}

		protected function log($msg, ...$args)
		{
			if (get_called_class() != __CLASS__) {
				// subordinate config change logged
				$msg = '  + ' . $msg;
			}

			return call_user_func($this->logger, $msg, ...$args);
		}

		protected function setPlatformState($version)
		{
			$this->_modifiedVersion = $version;
		}

		protected function getPlatformState()
		{
			return $this->_modifiedVersion;
		}

		abstract public function upgrade($conf): array;

		abstract public function downgrade($conf): array;
	}