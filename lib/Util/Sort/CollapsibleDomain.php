<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Util\Sort;

	/**
	 * Class CollapsibleDomain
	 *
	 * Collapse subdomains to parent
	 *
	 * @package Util\Sort
	 */
	class CollapsibleDomain
	{
		protected $seen = [
			// global subdomains
			'*' => 1
		];

		public function __invoke(string $a, string $b)
		{
			if (false === strpos($a, '.')) {
				$a .= '.*';
			}
			if (false === strpos($b, '.')) {
				$b .= '.*';
			}
			$a1 = explode('.', $a);
			$a2 = explode('.', $b);
			$c1 = \count($a1);
			$c2 = \count($a2);
			$len1 = $len2 = min($c1, $c2);
			$chk1 = substr($a, strpos($a, '.') + 1);
			$chk2 = substr($b, strpos($b, '.') + 1);
			if (isset($this->seen[$chk1])) {
				$len1 = substr_count($chk1, '.') + 1;
			}
			if (isset($this->seen[$chk2])) {
				$len2 = substr_count($chk2, '.') + 1;
			}
			if (!isset($this->seen[$a])) {
				$this->seen[$a] = 1;
			}
			if (!isset($this->seen[$b])) {
				$this->seen[$b] = 1;
			}
			// compare the domain components in common, then determine order
			// on subdomains unmatched
			$a1 = array_merge(\array_slice($a1, -$len1), \array_slice($a1, 0, $c1 - $len1));
			$a2 = array_merge(\array_slice($a2, -$len2), \array_slice($a2, 0, $c2 - $len2));
			while (true) {
				$s1 = (string)array_shift($a1);
				$s2 = (string)array_shift($a2);
				/*var_dump("HOST A: " . $a, "HOST B: " . $b, "RESULT: (" . strnatcmp($s1, $s2) . ") $s1 <=> $s2", $a1, $a2, "---");*/
				if (!$s1 && !$s2) {
					return 0;
				} else if (!$s1) {
					return -1;
				} else if (!$s2) {
					return 1;
				}
				if (0 !== ($cmp = strnatcmp($s1, $s2))) {
					return $cmp;
				}
			}
		}
	}