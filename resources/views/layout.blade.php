<!DOCTYPE html>
<html lang="en">
<head>
	@php
		$storage = $storagePercent = $bw = $bwPercent = null;
		if (\Auth::authenticated()) {
			if (UCard::get()->hasPrivilege('site')) {
				$bw = UCard::get()->getBandwidth();
				$bwPercent = $bw['total'] ? round(min(100, $bw['used'] / $bw['total'] * 100)) : 0;
			}
			$storage = UCard::get()->getStorage();
			$storagePercent = round(min(100, $storage['used'] / $storage['total'] * 100));
		}
@endphp
@include('theme::partials.head')
@stack('head')
</head>
<body class="{{ $Page->bodyClasses() }} @if (is_debug()) debug @endif nojs" id="app-{{ $Page->getApplicationID() }}">
<div class="d-flex flex-column" id="ui-full-flex-wrap">
	@php
		while (ob_get_level() > 1) {
			ob_end_flush();
		}
		flush();
	@endphp
	@includeWhen(Page_Renderer::do_header(), 'theme::partials.user.header')
	@include('theme::partials.app.content-wrapper')
	@include('theme::partials.app.modal')

	@includeWhen(\Auth::authenticated(), 'theme::partials.user.authenticated-footer')

	@foreach ($Page->get_javascript('external') as $js)
		<script type="text/javascript" src="{{ $js }}"></script>
	@endforeach

	<script type="text/javascript">
		@if ($scripts = $Page->get_javascript('internal', true, false))
			$(window).on("load", function() {
				{!! $scripts !!}
			});
		@endif
		{!! $Page->get_javascript('internal', false, false) !!}
	</script>

	@include('theme::partials.app.postback.ajax')
</div>
@stack('footer')
</body>
</html>
