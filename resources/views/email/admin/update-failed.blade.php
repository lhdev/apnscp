@component('email.indicator', ['status' => 'error'])
Update failed
@endcomponent

@component('mail::message')
# Hello,

Your recent update failed. See attached log for details.

---

**Server:** {{ SERVER_NAME }} ({{ \Opcenter\Net\IpCommon::my_ip() }})<br />
**Version:** {{ Opcenter::version() }}

---

@endcomponent

