@section("title", "Domain Added")
@component('mail::message')
Hello,<br /><br />

<p>
	A new domain has been created named <b>{{ $domain }}</b> under the account
	{{ $authdomain }} by user {{ $authuser }} (site ID {{ $siteid }}).
</p>
<p>
	{{ $domain }} path: {{ $path }}
</p>
@endcomponent