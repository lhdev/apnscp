@extends("email.auth.auth-common")
@section('title', "Hosting Account Notice")
@section('notice')
        This is to confirm that your account <b>{{ $what }}</b> has changed @if ($domain) for domain <b>{{ $domain }}</b> @endif
        @if ($username)(username: {{ $username }})@endif. <br /><br />
        @if (isset($password))
            Your new password is: <b>{{ $password }}</b>
            <br /><br />You may change the password within the control panel under <b>Account</b> &gt; <b>Change Information</b>.
            @component('mail::button', ['url' => \Auth_Redirect::getPreferredUri()])
                Access {{ PANEL_BRAND }}
            @endcomponent
        @else
            Please contact us immediately if you did not authorize this change.
        @endif
@endsection