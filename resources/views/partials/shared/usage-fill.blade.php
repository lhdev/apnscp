<div class="d-block text-right used py-1 px-1"
     style="background: linear-gradient(to left, #efefef 0, #efefef {{ ($percentage ?? 0) * 100 }}%, transparent {{ ($percentage ?? 0) * 100 }}%);" data-type="{{ \Illuminate\Support\Str::kebab(strtolower($title ?? "")) }}" data-value="{{ $used }}">
	@if ($srcunit ?? null)
		{{ round(Formatter::changeBytes($used, $unit, $srcunit)) }} {{ $unit }}
	@else
		{{ $used }} {{ $unit ?? '' }}
	@endif
</div>