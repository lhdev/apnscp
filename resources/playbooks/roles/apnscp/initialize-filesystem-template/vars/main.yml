---
synchronizer: "{{apnscp_root}}/bin/php-bins/php {{ apnscp_root }}/bin/scripts/yum-post.php"
relocated_paths:
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/php/pear"
    dest: /usr/share/pear
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/php/pear-data"
    dest: /usr/share/pear-data
    force: yes
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/perl/share"
    dest: /usr/share/perl5
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/perl/lib64"
    dest: /usr/lib64/perl5
    notify: Restart spamassassin
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/fcgi"
    dest: /var/tmp/fcgi
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/postfix"
    dest: /var/spool/postfix
    notify: Restart postfix
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/ssl/pki"
    dest: /etc/pki
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/ssl/pki/tls/certs"
    dest: /etc/ssl/certs
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/wtmp"
    dest: "/var/log/wtmp"
    logrotate_vars:
      file: /etc/logrotate.conf
      old: /var/log/wtmp
      new: "{{ apnscp_shared_root }}/wtmp"
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/btmp"
    dest: "/var/log/btmp"
    logrotate_vars:
      file: /etc/logrotate.conf
      old: /var/log/btmp
      new: "{{ apnscp_shared_root }}/btmp"
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/majordomo"
    dest: "/usr/lib/majordomo"
  - service: ssh
    src: "{{ apnscp_shared_root }}/php/usr/lib64/build"
    dest: "/usr/lib64/build"
    when: "{{ php_enabled }}"
  - service: ssh
    src: "{{ apnscp_shared_root }}/php/usr/include/php"
    dest: "/usr/include/php"
    when: "{{ php_enabled }}"
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/locale/share"
    dest: "/usr/share/locale"
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/locale/lib"
    dest: "/usr/lib/locale"
    when: ansible_distribution_major_version != 7
  - service: siteinfo
    src: "{{ apnscp_shared_root }}/gconv/lib"
    dest: "/usr/lib64/gconv"
  - service: tomcat
    src: "{{ apnscp_shared_root }}/tomcat"
    dest: /var/log/tomcat
# Files are copied on initial provisioning, not hardlinked nor updated after
oneshot_files:
  - {file: /etc/bashrc }
  - {file: /etc/csh.cshrc }
  - {file: /etc/csh.login }
  - {file: /etc/default/useradd }
  - {file: /etc/environment }
  - {file: /etc/filesystems }
  - {file: /etc/host.conf }
  - {file: /etc/resolv.conf }
  - {file: /etc/localtime }
  - {file: /etc/inputrc }
  -
    file: /etc/hosts.allow
    when: "{{ ansible_distribution_major_version == '7' }}"
  - {file: /etc/motd }
  - file: /etc/nsswitch.conf
    copy_links: True
  - {file: /etc/profile }
  - {file: /etc/protocols }
  -
    file: /etc/securetty
    when: "{{ ansible_distribution_major_version == '7' }}"
  - {file: /etc/services }
  - {file: /etc/shells }
  - {file: /etc/skel }
  - {file: /etc/sudoers }
  - {file: /etc/profile.d/lang.csh}
  - {file: /etc/profile.d/lang.sh}
  -
    file: /etc/profile.d/256term.csh
    when: "{{ ansible_distribution_major_version == '7' }}"
  -
    file: /etc/profile.d/256term.sh
    when: "{{ ansible_distribution_major_version == '7' }}"
  - {file: /usr/share/empty}
  - {file: /usr/bin/composer}

apnscp_fst_packages:
  - service: siteinfo
    packages:
      - name: acl
      - name: alsa-lib
      - name: apr-util
      - name: apr
      - name: aspell
      - name: audit-libs
      - name: authconfig
      - name: avahi-libs
      - name: bash-completion
      - name: bash
      - name: bc
      - name: bind-libs
      - name: bind-libs-lite
      - name: bind-utils
      - name: boost
      - name: brotli
      - name: bzip2-libs
      - name: bzip2
      - name: ca-certificates
      - name: cairo
      - name: c-ares
      - name: chkconfig
      - name: "{{ (ansible_distribution_major_version == '7') | ternary('compat-libtidy', 'libtidy') }}"
      - name: coreutils
      - name: coreutils-common
        when: "{{ (ansible_distribution_major_version != '7') }}"
      - name: courier-unicode
      - name: cpio
      - name: cracklib-dicts
      - name: cracklib
      - name: crontabs
      - name: crypto-policies
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: cups-libs
      - name: curl
      - name: cyrus-sasl-lib
      - name: cyrus-sasl
      - name: dbus-libs
      - name: diffutils
      - name: dos2unix
      - name: e2fsprogs-libs
      - name: e2fsprogs
      - name: ed
      - name: elfutils-libelf
      - name: elfutils-libs
      - name: expat
      - name: fcgi
      - name: fetchmail
      - name: fftw-libs-double
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: file-libs
      - name: file
      - name: findutils
      - name: fontconfig
      - name: freetype
      - name: fribidi
      - name: fstrm
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: ftp
      - name: gawk
      - name: gdbm
      - name: gdbm-libs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: gd
      - name: GeoIP
      - name: gettext-libs
      - name: gettext
      - name: ghostscript-fonts
        when: "{{ (ansible_distribution_major_version == '7') }}"
      - name: ghostscript
      - name: giflib
      - name: git
      - name: git-core
        when: "{{ (ansible_distribution_major_version != '7') }}"
      - name: glib2
      - name: glibc-common
      - name: glibc-static
      - name: glibc
      - name: gmp
      - name: gnutls-c++
      - name: gnutls-dane
      - name: gnutls
      - name: gpgme
      - name: graphite2
      - name: grep
      - name: groff-base
      - name: groff
        when: "{{ (ansible_distribution_major_version != ansible_distribution_version) }}"
      - name: gsl
      - name: gzip
      - name: harfbuzz
      - name: hesiod
      - name: httpd-tools
      - name: http-parser
      - name: icu
      - name: ImageMagick-c++
      - name: ImageMagick-perl
      - name: ImageMagick
      - name: ImageMagick-libs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: info
      - name: iputils
      - name: jasper-libs
      - name: jbigkit-libs
      - name: js
        when: "{{ (ansible_distribution_major_version == '7') }}"
      - name: json-c
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: jwhois
        when: "{{ (ansible_distribution_major_version == '7') }}"
      - name: keyutils-libs
      - name: krb5-libs
      - name: lcms2
      - name: less
      - name: libacl
      - name: libaio
      - name: libargon2
      - name: libattr
      - name: libblkid
      - name: libcap-ng
      - name: libcap
      - name: libc-client
      - name: libcgroup-pam
      - name: libcgroup-tools
      - name: libcgroup
      - name: libcom_err
      - name: libcroco
      - name: libcurl
      - name: libdb4
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: libdb
      - name: libevent
      - name: libffi
      - name: libgcc
      - name: libgcrypt
      - name: libgfortran
      - name: libgomp
      - name: libgpg-error
      - name: libgs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libICE
      - name: libicu
      - name: libidn
      - name: libidn2
      - name: libjpeg-turbo
      - name: libmaxminddb
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libmcrypt
      - name: libmemcached-libs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libmetalink
      - name: libmount
      - name: libmpc
      - name: libnghttp2
      - name: libnsl2
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libpipeline
      - name: libpng
      - name: libpsl
      - name: libpwquality
      - name: libquadmath
      - name: libraqm
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libselinux
      - name: libsemanage
      - name: libsepol
      - name: libsigsegv
        when: "{{ (ansible_distribution_major_version != '7') }}"
      - name: libsmartcols
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libsodium
      - name: libSM
      - name: libssh
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libssh2
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: libss
      - name: libstdc++
      - name: libtasn1
      - name: libtiff
      - name: libtirpc
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libtool-ltdl
      - name: libunistring
      - name: libunwind
      - name: libusb
      - name: libuser
      - name: libutempter
      - name: libuuid
      - name: libwebp
      - name: libX11
      - name: libXau
      - name: libxcb
      - name: libxcrypt
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libXdmcp
      - name: libXext
      - name: "{{ (ansible_distribution_major_version == '7') | ternary('libxml2-python', 'python3-libxml2')}}"
      - name: libxml2
      - name: libXmu
      - name: libXpm
      - name: libXrender
      - name: libxslt-python
        when: "{{ (ansible_distribution_major_version == '7') }}"
      - name: libxslt
      - name: libXt
      - name: libyaml
      - name: libzip
      - name: lm_sensors-libs
      - name: logrotate
      - name: lua
      - name: lua-libs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: lz4
      - name: lz4-libs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: maildrop
      - name: mailx
      - name: man-db
      - name: man-pages
      - name: MariaDB-client
      - name: MariaDB-common
      - name: MariaDB-shared
      - name: libpmem
        when: "{{ ansible_distribution_version != '7' and mariadb_version is version('10.6', '>=') }}"
      - name: mpfr
      - name: ncftp
      - name: ncurses-libs
      - name: ncurses
      - name: net-snmp-agent-libs
      - name: net-snmp-libs
      - name: net-snmp
      - name: nettle
      - name: net-tools
      - name: nspr
      - name: nss-pem
        when: "{{ (ansible_distribution_major_version == '7') }}"
      - name: nss-softokn-freebl
      - name: nss-softokn
      - name: nss-sysinit
      - name: nss-util
      - name: nss
      - name: oniguruma
      - name: openblas-serial
      - name: "openjpeg{{ (ansible_distribution_major_version == '7') | ternary('-libs', '2') }}"
      - name: openldap-devel
      - name: openldap
      - name: openssl-libs
      - name: openssl
      - name: p11-kit-trust
      - name: p11-kit
      - name: pam-devel
      - name: pam
      - name: pam-apnscp
      - name: passwd
      - name: patch
      - name: pcre
      - name: pcre2
      - name: pcre32-utf
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: perl
      - name: perl-CPAN
      - name: perl-CGI
      - name: perl-interpreter
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: perl-libs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: platform-python
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: platform-python-setuptools
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: pixman
      - name: poppler
      - name: popt
      - name: postfix
      - name: "postgresql{{pg_pkgversion}}-contrib"
      - name: "postgresql{{pg_pkgversion}}-devel"
      - name: "postgresql{{pg_pkgversion}}-libs"
      - name: procps-ng
      - name: protobuf-c
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: "{{ (ansible_distribution_major_version == '7') | ternary('pth', 'npth') }}"
      - name: "{{ (ansible_distribution_major_version == '7') | ternary('', 'python3-')}}pyOpenSSL"
      - name: "python{{ (ansible_distribution_major_version == '7') | ternary('2-crypto', '3-cryptography')}}"
      - name: "python{{ (ansible_distribution_major_version == '7') | ternary('2', '3')}}-pip"
      - name: python-backports-ssl_match_hostname
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: python-backports
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: "python{{ (ansible_distribution_major_version == '7') | ternary('', '3')}}-libs"
      - name: "python{{ (ansible_distribution_major_version == '7') | ternary('', '3')}}-setuptools"
      - name: "python{{ (ansible_distribution_major_version == '7') | ternary('', '36')}}"
      - name: pyzor
      - name: qrencode-libs
      - name: qrencode
      - name: quota
      - name: readline
      - name: rpm-libs
      - name: sed
      - name: setup
      - name: shadow-utils
      - name: source-highlight
      - name: sqlite
      - name: sqlite-libs
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: systemd-libs
      - name: sysvinit-tools
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: tar
      - name: tcl
      - name: tcp_wrappers-libs
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: tcp_wrappers
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: tcsh
      - name: telnet
      - name: tmpwatch
      - name: traceroute
      - name: tree
      - name: trousers
      - name: tzdata
      - name: unixODBC
      - name: unzip
      - name: urw-base35-fonts
      - name: ustr
      - name: util-linux
      - name: wget
      - name: which
      - name: xz-devel
      - name: xz-libs
      - name: xz
      - name: yaml-cpp
      - name: zip
      - name: zlib
  - service: ssh
    packages:
      - name: aspell-devel
      - name: atlas
      - name: autoconf213
      - name: autoconf
      - name: automake
      - name: binutils
      - name: bison
      - name: boost-atomic
      - name: boost-chrono
      - name: boost-context
      - name: boost-date-time
      - name: boost-devel
      - name: boost-filesystem
      - name: boost-graph
      - name: boost-iostreams
      - name: boost-locale
      - name: boost-math
      - name: boost-program-options
      - name: "boost-python{{ (ansible_distribution_major_version == '7') | ternary('', '3')}}"
      - name: boost-random
      - name: boost-regex
      - name: boost-serialization
      - name: boost-signals
      - name: boost-system
      - name: boost-test
      - name: boost-thread
      - name: boost-timer
      - name: boost-wave
      - name: byacc
      - name: bzip2-devel
      - name: c-ares-devel
      - name: "{{ (ansible_distribution_major_version == '7') | ternary('compat-libtidy-devel', 'libtidy-devel') }}"
      - name: cpp
      - name: cronie
      - name: cyrus-sasl-devel
      - name: e2fsprogs-devel
      - name: elinks
      - name: emacs-common
      - name: emacs-mercurial
      - name: emacs-nox
      - name: expat-devel
      - name: expect
      - name: file-devel
      - name: fipscheck-lib
      - name: freetype-devel
      - name: gcc-c++
      - name: gcc
      - name: gd-devel
      - name: GeoIP-devel
      - name: gettext-devel
      - name: git-core-doc
        when: "{{ (ansible_distribution_major_version != '7') }}"
      - name: libgs-devel
      - name: giflib-devel
      - name: git-daemon
      - name: glibc-devel
      - name: glibc-headers
      - name: glibc-utils
      - name: gmp-devel
      - name: gnutls-devel
      - name: gperftools-libs
      - name: gpm-libs
      - name: gsl-devel
      - name: gyp
      - name: http-parser-devel
      - name: ImageMagick-c++-devel
      - name: ImageMagick-devel
      - name: jemalloc
      - name: kernel-headers
      - name: krb5-devel
      - name: lcms2-devel
      - name: libargon2-devel
      - name: libassuan
      - name: libblkid-devel
      - name: libcom_err-devel
      - name: libcurl-devel
      - name: libdb4-devel
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: libdb-devel
      - name: libdrm
      - name: libedit
      - name: libffi-devel
      - name: libgcrypt-devel
      - name: libgpg-error-devel
      - name: libICE-devel
      - name: libicu-devel
      - name: libidn-devel
      - name: libidn2-devel
      - name: libjpeg-turbo-devel
      - name: libmcrypt-devel
      - name: libmemcached-devel
      - name: libmemcached
      - name: libmodman
      - name: libnghttp2-devel
      - name: libpcap
      - name: libpng-devel
      - name: libproxy
      - name: libpsl-devel
        when: "{{ (ansible_distribution_major_version == '7') }}"
      - name: libserf
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libSM-devel
      - name: libsodium-devel
      - name: libstdc++-devel
      - name: libstemmer
      - name: libtasn1-devel
      - name: libtool-ltdl-devel
      - name: libtool
      - name: libuuid-devel
      - name: libwebp-devel
      - name: libxcrypt-devel
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: libXdamage
      - name: libXext-devel
      - name: libXfixes
      - name: libxml2-devel
      - name: libXmu-devel
      - name: libXpm-devel
      - name: libXrender-devel
      - name: libxslt-devel
      - name: libXt-devel
      - name: libXxf86vm
      - name: libyaml-devel
      - name: libzip-devel
      - name: lm_sensors-devel
      - name: lsof
      - name: m4
      - name: make
      - name: MariaDB-devel
      - name: memcached
      - name: mercurial
      - name: mesa-libEGL
      - name: mesa-libgbm
      - name: mesa-libglapi
      - name: mesa-libGL
      - name: mongodb-org-mongos
        when: '{{ mongodb_enabled | bool }}'
      - name: mongodb-org-server
        when: '{{ mongodb_enabled | bool }}'
      - name: mongodb-org-shell
        when: '{{ mongodb_enabled | bool }}'
      - name: mongodb-org-tools
        when: '{{ mongodb_enabled | bool }}'
      - name: mongodb-org
        when: '{{ mongodb_enabled | bool }}'
      - name: mutt
      - name: nano
      - name: ncurses-base
      - name: ncurses-devel
      - name: neon-devel
      - name: neon
      - name: net-snmp-devel
      - name: nspr-devel
      - name: nss_compat_ossl
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: oniguruma-devel
      - name: "openjpeg{{ (ansible_distribution_major_version == '7') | ternary('', '2') }}-devel"
      - name: openssh-clients
      - name: openssh
        # sftp
      - name: openssh-server
      - name: openssl-devel
      - name: p11-kit-devel
      - name: pakchois
      - name: pcre-devel
      - name: pcre2-devel
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: pixman-devel
      - name: pkgconfig
      - name: "postgresql{{pg_pkgversion}}"
      - name: pth-devel
        when: "{{ ansible_distribution_major_version == '7' }}"
      - name: "python{{ (ansible_distribution_major_version == '7') | ternary('', '3')}}-devel"
      - name: "python{{ (ansible_distribution_major_version == '7') | ternary('', '3')}}-virtualenv"
      - name: readline-devel
      - name: redis
      - name: rsync
      - name: screen
      - name: snappy
      - name: sqlite-devel
      - name: strace
      - name: subversion-devel
      - name: subversion-libs
      - name: subversion
      - name: tcl-devel
      - name: tmux
      - name: tokyocabinet
      - name: unixODBC-devel
      - name: utf8proc
        when: "{{ ansible_distribution_major_version != '7' }}"
      - name: vim-common
      - name: vim-enhanced
      - name: vim-minimal
      - name: Xaw3d
      - name: xmlto
      - name: zlib-devel
      - name: zsh
  - service: tomcat
    packages:
      - name: "{{ (ansible_distribution_major_version == '7') | ternary('java-1.7.0-openjdk', 'java-latest-openjdk')}}"
      - name: "{{ (ansible_distribution_major_version == '7') | ternary('java-1.7.0-openjdk-headless', 'java-latest-openjdk-headless')}}"
      - name: javapackages-tools
