#!/usr/bin/env apnscp_php
<?php declare(strict_types=1);
	/**
	  * Usage: change_dns.php -d domain [--old=IP] [NEWIP]
	  */

	use Opcenter\Net\Ip4;
	use Opcenter\Net\Ip6;

	include __DIR__ .'/../../lib/CLI/cmd.php';
	Error_Reporter::set_verbose(1);
	$opts = [];
	$args = \cli\parse($opts, 'h', ['old:', 'new:', 'ttl:', 'all', 'help']);

	if (isset($opts['help']) || isset($opts['h']) || isset($args[0])) {
		help();
	}

	if (isset($opts['all'])) {
		$sites = array_keys(\apnscpFunctionInterceptor::init()->admin_collect(['dns.enabled' => 1]));
	} else {
		$afi = \cli\get_instance();
		if (!\Auth::profile()->domain) {
			help();
		}
		$sites = [\Auth::profile()->site];
	}

	foreach ($sites as $site) {
		if (null === ($ctx = Auth::nullableContext(null, $site))) {
			continue;
		}

		$afi = \apnscpFunctionInterceptor::factory($ctx);
		$myips = implode(',', (array)$afi->dns_get_public_ip() + append_config((array)$afi->dns_get_public_ip6()));
		$newip = $opts['new'] ?? '';

		if ($newip && !isset($opts['old'])) {
			// --new=XYZ - performing IP address change, inherit service definition
			$type = false !== strpos($newip, ':') ? 'ip4' : 'ip6';
			$fn = 'dns_get_public_' . $type;
			$oldip = $afi->$fn();
		} else {
			// --old=XYZ or performing DNS reduction
			$oldip = preg_split('/,/', $opts['old'] ?? $myips, -1, PREG_SPLIT_NO_EMPTY);
		}

		if ($newip && !Ip4::valid($newip) && !Ip6::valid($newip)) {
			fatal("Invalid NEWIP passed: %s", $newip);
		}

		$domains = array_keys($afi->web_list_domains());

		if (isset($opts['ttl'])) {
			$ttl = (int)$opts['ttl'];
		} else {
			$ttl = $newip ? $afi->dns_get_default('ttl') : 60;
		}
		foreach ((array)$oldip as $ip) {
			change($afi, $domains, $ip, $newip, $ttl);
		}
	}

	function change(\apnscpFunctionInterceptor $afi, array $domains, string $old, string $new = '', ?int $ttl = null)
	{
		static $recordCache = [];
		$what = 'prepped for change';
		if ($new) {
			$what = 'changed';
		}
		$rr = false === strpos($old, ':') ? 'A' : 'AAAA';
		foreach ($domains as $domain) {
			if (!$afi->dns_zone_exists($domain)) {
				warn("Ignoring %s - zone not handled by nameserver", $domain);
				continue;
			}
			$key = "$rr.$domain";
			if (!array_has($recordCache, $key)) {
				array_set($recordCache, $key, $afi->dns_get_records_by_rr($rr, $domain));
			}
			$records = array_get($recordCache, $key);
			foreach ($records as $r) {
				if ($r['parameter'] !== $old) {
					continue;
				}
				$newparams = [
					'ttl'        => $ttl,
					'parameter'  => $new ?: $old
				];
				$hostname = ltrim($r['subdomain'] . '.' . $r['domain'], '.');
				if (!$afi->dns_modify_record($r['domain'], $r['subdomain'], $rr, $old, $newparams)) {
					error("failed to modify record for `" . $hostname . "' (%s)", $rr);
					continue;
				}
				info('%s %s', $what, $hostname);
			}
		}
	}

	function help() {
		echo 'Usage: ' . basename($_SERVER['argv'][0]) . " [-d DOMAIN|--all] [--ttl=TTL] [--old=IP,IP2,...] [--new=IP]" . "\n" .
			str_repeat('-', 80) . "\n" .
			'Apply changes to all domains using --all or target specific account with -d DOMAIN' . "\n\n" .
			'Reduce TTL for all IPs that match OLDIP: --old=OLDIP ' . "\n" .
			'Reduce TTL on all IPs that match account IP: -d DOMAIN or --all' . "\n" .
			'Change service definition IP to new IP: --new=IP' . "\n" .
			'Change all IPs that match OLDIP to NEWIP: --old=OLDIP --new=NEWIP' . "\n" .
			'--ttl=TTL overrides recommended TTL (prep: 60 seconds/IP change: module default)' . "\n\n";
		exit(1);
	}