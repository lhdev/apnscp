#!/usr/bin/env apnscp_php
<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use CLI\Transfer\CliParser;

	ini_set('default_socket_timeout', "180");
    error_reporting(E_ALL);
	define('INCLUDE_PATH', dirname(__DIR__, 2));
    include(INCLUDE_PATH.'/lib/CLI/cmd.php');
    include(INCLUDE_PATH.'/lib/CLI/Transfer.php');
    define('ADMIN_API_KEY', get_admin_key());

	function get_admin_key() {
        $mockup = \Auth::context(null);
        $api = \apnscpFunctionInterceptor::factory($mockup);
        $keys = $api->auth_get_api_keys();
        if (count($keys) < 1) {
            return $api->auth_create_api_key('transfers') || fatal("failed to create admin api key");
        }
        return array_get(array_pop($keys), 'key', null);
    }

	[$options, $sites] = array_values(CliParser::parse());

	if (!$sites) {
		CliParser::usage();
	}

	// admin just needs remote API access vb

	foreach ($sites as $site) {
		$domain = \Auth::get_domain_from_site_id(substr($site, 4));
		$_ENV['DOMAIN'] = $domain;

		if (null === ($ctx = \Auth::nullableContext(null, $site))) {
			dlog("Skipping %s - error on initializing context", $site);
			continue;
		}

		$xfer = CLI_Transfer::instantiateContexted($ctx, [$options]);
		if ($options['override']) {
			$xfer->setOverrides($options['override']);
		}

		if (!$options['create']) {
			dlog("Skipping domain creation");
			$xfer->skipCreateDomain();
		}

		if ($options['force']) {
			dlog("Forcing migration ahead of 24 hour sanity check");
			$xfer->force();
		}

		if (!is_null($options['stage'])) {
			dlog("Setting migration stage `%d'", $options['stage']);
			$xfer->setStage($options['stage']);
		}

		if ($options['server']) {
			dlog("Setting server `%s'", $options['server']);
			$xfer->setTargetServer($options['server']);
		}

		if ($options['do']) {
			foreach ((array)$options['do'] as $what) {
				$args = explode(",", $what, 2);
				$what = array_shift($args);
				$args = $args ? \Opcenter\CliParser::parseArgs($args[0]) : null;
				$xfer->addStage($what, $args);
			}
		}

		if ($options['pull']) {
			$xfer->flip();
		}

		if (!isset($options['no-suspend']) && !$xfer->suspend()) {
			warn("Error on suspending %s", $domain);
		}
		// second stage
		if ($options['notify'] && !$xfer->notify() && CLI_Transfer::STATUS_EMAIL) {
			Mail::send(
				CLI_Transfer::STATUS_EMAIL,
				"Migration Notification Failed - " . $domain,
				join("\n", Error_Reporter::get_errors()),
				"From: " . Crm_Module::FROM_NAME . " <" . Crm_Module::FROM_ADDRESS . ">"
			);
		}
		if ($options['log']) {
			(new \CLI\Output('line'))->setDescriptor(\CLI\Output::STDOUT)->fromLog(\Error_Reporter::flush_buffer())->echo("DONE");
		}
		exit(0);

		try {
			$initialStage = $xfer->migrationStage();
			$xfer->process();
		} catch (Exception $e) {
			$reason = $e->getMessage();
			if ($reason === "Unauthorized") {
				$reason .= " - update domain auth table?";
			}

			error("migration failed - unhandled exception");
		}

		dlog("Transfer %s (%s) %s",
			$domain,
			Error_Reporter::error_type(Error_Reporter::get_severity()),
			Error_Reporter::is_error() ? 'Failed' : 'Succeeded'
		);

		$currentStage = $xfer->migrationStage();

		if ($xfer->incomplete()) {
			error("Error encountered during %s migration. Migration is incomplete!", $domain);
		} else if ($xfer->migrated()) {
			if (!isset($options['no-suspend']) && !$xfer->suspend()) {
				warn("Error on suspending %s", $domain);
			}
			// second stage
			if ($options['notify'] && !$xfer->notify() && CLI_Transfer::STATUS_EMAIL)
			{
				Mail::send(
					CLI_Transfer::STATUS_EMAIL,
					"Migration Notification Failed - " . $domain,
					join("\n", Error_Reporter::get_errors()),
					"From: " . Crm_Module::FROM_NAME . " <" . Crm_Module::FROM_ADDRESS . ">"
				);
			}
		}

		\Error_Reporter::print_buffer();

		if ($options['log']) {
			(new \CLI\Output('line'))->setDescriptor(\CLI\Output::STDOUT)->fromLog(\Error_Reporter::flush_buffer())->echo("DONE");
		}
	}


    exit (\Error_Reporter::is_error());