<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class ContextabilityConcurrencyTest extends TestFramework
    {
        protected $filename;

		/**
		 *
		 */
		public function testContextConcurrency()
		{
			$auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$afi1 = apnscpFunctionInterceptor::factory($auth1);

			$auth2 = \TestHelpers::create(array_get(Definitions::get(), 'auth.contextable.domain'));
			$afi2 = apnscpFunctionInterceptor::factory($auth2);
			$this->assertNotEquals(spl_object_hash($afi1), spl_object_hash($afi2));
			$this->assertNotEquals($auth1->id, $auth2->id);
			$this->assertEquals($auth1->level, $auth2->level);
			$this->assertTrue($afi2->context_matches_id($auth2->id));
			$this->assertFalse($afi2->context_matches_id($auth1->id));

			$this->assertNotEquals($auth1->username, $auth2->username);
		}

		/**
		 * Setting another language for a secondary user
		 */
		public function testLanguage() {
			$context = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$defaultLanguage = array_get(\Preferences::factory($context), 'language', 'en_US');
			$otherLanguage = $defaultLanguage === 'en_US' ? 'de_DE' : 'en_US';
			$context2 = TestHelpers::createUser($context->getAccount());
			$this->assertNotEmpty($context2);
			$afi2 = \apnscpFunctionInterceptor::factory($context2);
			$prefs = \Preferences::factory($context2);
			$prefs->unlock($afi2);
			$prefs['language'] = $otherLanguage;
			$prefs->sync();
			$this->assertEquals($otherLanguage, array_get($afi2->common_load_preferences(), 'language'), 'Language correctly set for contexted user');
		}
    }

