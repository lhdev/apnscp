<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class JoomlaTest extends TestFramework
	{
		const VERSION = '3.8.9';
		const VERSION_NEXT = '3.8.10';
		const EXPECTED_UPGRADE_VERSION = '3.8.13';
		const INSTALL_VERSION = self::VERSION;
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testVersionFetch()
		{
			$afi = \apnscpFunctionInterceptor::init();
			$versions = $afi->joomla_get_versions();
			$this->assertNotEmpty($versions, 'Version check succeeded');
			$this->assertContains(self::VERSION, $versions, self::VERSION . ' in version index');
			$this->assertGreaterThan(array_search(self::VERSION, $versions), array_search(self::VERSION_NEXT, $versions), 'Versions are ordered');
		}

		public function testInstall()
		{
			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;

			$this->assertTrue(
				$afi->joomla_install(
					$domain,
					'',
					[
						'version' => self::INSTALL_VERSION,
						'notify'  => false,
						'ssl'     => false,
						'verlock' => 'minor',
					]
				)
			);

			$this->assertEquals(self::INSTALL_VERSION, $afi->joomla_get_version($domain),
				'Correct Joomla! version is installed');

			$this->assertTrue($afi->joomla_update_all($domain, '', self::EXPECTED_UPGRADE_VERSION), 'Joomla! updates');
			$this->assertEquals(self::EXPECTED_UPGRADE_VERSION, $afi->joomla_get_version($domain),
				'Joomla! upgrades to correct version');
		}
	}

