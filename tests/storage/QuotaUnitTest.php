<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class QuotaUnitTest extends TestFramework
	{
		public function testIrregularUnits()
		{
			// Quota regularly in MB
			$auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$afi1 = apnscpFunctionInterceptor::factory($auth1);
			$old = $afi1->site_get_account_quota();
			$quota = mt_rand(4096, 10240) * 1024;
			$editor = new Util_Account_Editor($auth1->getAccount(), $auth1);
			$editor->setConfig([
				'diskquota,enabled' => true,
				'diskquota,quota' => $quota,
				'diskquota,units' => 'KB'
			]);
			$this->assertTrue($editor->edit(), 'Edit succeeded');
			$new = $afi1->site_get_account_quota();
			$this->assertNotSame($old, $new, 'Quota change effected');
			$this->assertEquals($quota, $new['qhard'], 'Hard quota matches value');
		}
	}

