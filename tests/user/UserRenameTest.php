<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class UserRenameTest extends TestFramework
	{

		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testUserRename()
		{
			$account = \Opcenter\Account\Ephemeral::create(['mail.enabled' => true, 'mail.provider' => 'builtin']);
			$afi = $account->getApnscpFunctionInterceptor();
			$this->assertTrue(
				$afi->user_add(
					'bad-user-123',
					\Opcenter\Auth\Password::generate(),
					'',
					500,
					['cp' => true, 'imap' => true, 'smtp' => true]
				)
			);
			$this->assertArrayHasKey('bad-user-123', $afi->user_get_users());

			$this->assertTrue(
				$afi->web_add_subdomain('bad-test-123', $afi->user_get_home('bad-user-123') . '/public_html')
			);
			$this->assertGreaterThan(0, $uid = $afi->user_get_uid_from_username('bad-user-123'));
			$this->assertTrue(
				$afi->email_add_mailbox(
					'bad-user-123',
					$account->getContext()->domain,
					$uid
				)
			);

			$this->assertTrue($afi->email_address_exists('bad-user-123', $account->getContext()->domain));

			$afi->user_rename_user('bad-user-123', 'bad-user-456');

			$this->assertTrue($afi->email_address_exists('bad-user-456', $account->getContext()->domain));

			$this->assertTrue($account->destroy(), 'Destroy account');
		}
	}

