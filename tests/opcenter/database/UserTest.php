<?php
	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	/**
	 * Class User
	 *
	 * Handling of new MariaDB methods
	 */
	class UserTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function setUp()
		{
			parent::setUp();
			if (\Opcenter\Database\MySQL::version() < Mysql_Module::NEW_API_VERSION) {
				return $this->markTestSkipped(sprintf('MariaDB 10.2 required. %s found', \Opcenter\Database\MySQL::version()));
			}
		}

		public function testCreateUser()
		{
			$user = \Opcenter\Auth\Password::generate(16, 'a-z');
			$this->assertTrue(
				\Opcenter\Database\MySQL::createUser(
					$user,
					\Opcenter\Auth\Password::generate(),
					'localhost'
				),
				'User creation'
			);

			$this->assertTrue(
				\Opcenter\Database\MySQL::deleteUser($user, 'localhost'),
				'User deletion'
			);
		}

		public function testWildcard() {
			$db = $password = $user = \Opcenter\Auth\Password::generate(16, 'a-z');
			$ctx = TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$afi = apnscpFunctionInterceptor::factory($ctx);
			$this->assertTrue($afi->mysql_add_user($user, 'localhost', $password), 'User created');
			$this->assertTrue($afi->mysql_create_database($db), 'Database created');

			try {
				$this->assertEmpty(array_filter($afi->mysql_get_privileges($user, 'localhost', $db)),
					'User lacks grants');
				$this->assertTrue($afi->mysql_set_privileges($user, 'localhost', '%', ['write' => true]),
					'Wildcard privileges applied');
				$this->assertNotEmpty(array_filter($afi->mysql_get_privileges($user, 'localhost', $db)),
					'Wildcard grants applied');

				\Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);

				$afi->mysql_set_privileges($user, 'localhost', $db, ['write' => true]);
				$this->fail('Altering privileges on wildcard must fail');
			} catch (\apnscpException $e) {

			} finally {
				$afi->mysql_user_exists($user, 'localhost') && $afi->mysql_delete_user($user, 'localhost');
				$afi->mysql_database_exists($db) && $afi->mysql_delete_database($db);
			}
			return true;
		}

		public function testCreateUserOptions() {
			$user = \Opcenter\Auth\Password::generate(16, 'a-z');
			$this->assertTrue(
				\Opcenter\Database\MySQL::createUser(
					$user,
					\Opcenter\Auth\Password::generate(16),
					'localhost',
					[
						'issuer' => '/C=SE/ST=Stockholm/L=Stockholm/O=MySQL/CN=CA/emailAddress=ca@example.com',
						'subject' => '/C=SE/ST=Stockholm/L=Stockholm/O=MySQL demo client certificate/CN=client/emailAddress=client@example.com'
					],
					[
						'connections' => 12,
						'query' => 100,
						'updates' => 100,
					]
				),
				'User creation'
			);

			$this->assertTrue(
				\Opcenter\Database\MySQL::deleteUser($user, 'localhost'),
				'User deletion'
			);
		}

		public function testUserOptionChange()
		{
			$user = \Opcenter\Auth\Password::generate(16, 'a-z');
			$this->assertTrue(
				\Opcenter\Database\MySQL::createUser(
					$user,
					\Opcenter\Auth\Password::generate(),
					'localhost',
					[],
					[
						'connections' => 12,
						'query'       => 100,
						'updates'     => 100,
					]
				),
				'User creation'
			);

			defer($_, function () use ($user) {
				$this->assertTrue(
					\Opcenter\Database\MySQL::deleteUser($user, 'localhost'),
					'User deletion'
				);
			});

			$this->assertTrue(
				\Opcenter\Database\MySQL::alterUser($user, 'localhost', [
					'connections' => 5,
					'query' => 0,
					'password' => 'foo'
				])
			);

			$this->assertNull((new mysqli("localhost", $user, 'foo'))->connect_error, 'Password change succeeded');
		}

		public function testCreateDatabase()
		{
			$user = \Opcenter\Auth\Password::generate(16, 'a-z');
			$this->assertTrue(
				\Opcenter\Database\MySQL::createUser(
					$user,
					\Opcenter\Auth\Password::generate(),
					'localhost'
				),
				'User creation'
			);

			$db = \Opcenter\Auth\Password::generate(16, 'a-z');
			$this->assertTrue(
				\Opcenter\Database\MySQL::createDatabase($db, $user, 'localhost')
			);
			$this->assertTrue(
				\Opcenter\Database\MySQL::deleteUser($user, 'localhost'),
				'User deletion'
			);

			$this->assertTrue(
				\Opcenter\Database\MySQL::dropDatabase($db),
				'Drop database'
			);
		}

		public function testRenameUser()
		{
			$user = \Opcenter\Auth\Password::generate(16, 'a-z');
			$this->assertTrue(
				\Opcenter\Database\MySQL::createUser($user, \Opcenter\Auth\Password::generate(), 'localhost'),
				'User creation'
			);
			$this->assertTrue(
				\Opcenter\Database\MySQL::renameUser($user, $user . 'abc'),
				'User rename'
			);
			$this->assertTrue(
				\Opcenter\Database\MySQL::deleteUser($user . 'abc'),
				'User deletion'
			);

		}

	}

