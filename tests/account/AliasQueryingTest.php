<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class AliasQueryingTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_FATAL;

		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testDirectAlias() {
			$addonDomain = \Opcenter\Auth\Password::generate(16, 'a-z0-9') . '.test';
			$account = \Opcenter\Account\Ephemeral::create([
				'aliases.max' => null,
				'aliases.aliases' => [$addonDomain]
			]);
			$afi = $account->getApnscpFunctionInterceptor();
			$this->assertSame(Web_Module::MAIN_DOC_ROOT, $afi->web_get_docroot($addonDomain));
			$this->assertIsArray($components = $afi->web_split_host("www.$addonDomain"));
			$this->assertSame(['www', $addonDomain], array_values($components));
			$this->assertTrue($afi->web_domain_exists($addonDomain));
			$this->assertTrue($afi->aliases_domain_exists($addonDomain));
		}
	}

