<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class AddonDomainAliasesUpdate extends TestFramework
	{

		protected ?\Opcenter\Account\Ephemeral $account;
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function __construct()
		{
			parent::__construct();
			$this->account = \Opcenter\Account\Ephemeral::create([
				'aliases.max' => null
			]);
		}

		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testAliasesUpdate() {
			/**
			 * via Discord:
			 *
			 * Think I just found a bug
			 * If you create a user in Nexus, then login as that user and create an addon domain,
			 * then logout as that user, go back to Nexus, change their aliases, log back in is that user,
			 * they'll be unable to delete the existing addon domain as the alias changed.
			 */
			$afi = $this->account->getApnscpFunctionInterceptor();
			$testDomain = \Opcenter\Auth\Password::generate(16, 'a-z0-9')  . '.test';
			$afi->aliases_add_domain($testDomain, '/var/www/somepath');
			$afi->aliases_synchronize_changes();
			$this->assertContains($testDomain, $this->account->getContext()->getAccount()->conf['aliases']['aliases'], 'New domain in aliases');
			$this->assertArrayHasKey($testDomain, $afi->aliases_list_shared_domains());
			$editor = new Util_Account_Editor($this->account->getContext()->getAccount(), $this->account->getContext());
			$editor->setConfig('aliases', 'aliases', []);
			$this->assertTrue($editor->edit());
			$this->assertNotContains($testDomain, $this->account->getContext()->getAccount()->conf['aliases']['aliases'],
				'New domain removed from aliases');
			$this->assertArrayNotHasKey($testDomain, $afi->aliases_list_shared_domains());
		}

		public function testAliasRemoval()
		{
			$afi = $this->account->getApnscpFunctionInterceptor();
			$testDomain = \Opcenter\Auth\Password::generate(16, 'a-z0-9') . '.test';
			$afi->aliases_add_domain($testDomain, '/var/www/test');
			$afi->aliases_synchronize_changes();
			$map = \Opcenter\Http\Apache\Map::fromSiteDomainMap($this->account->getContext()->domain_info_path(), $this->account->getContext()->site);
			$this->assertTrue(isset($map[$testDomain]));
			$sdbm = $map->getOutput() . $map->getExtensionFromType();
			unset($map);
			$this->assertNotFalse(strpos(file_get_contents($sdbm), $testDomain));
			$afi->web_add_subdomain($testDomain, "/var/www/foo");
			$afi->aliases_remove_domain($testDomain);
			$afi->aliases_synchronize_changes();
			$map = \Opcenter\Http\Apache\Map::fromSiteDomainMap($this->account->getContext()->domain_info_path(),
				$this->account->getContext()->site, \Opcenter\Http\Apache\Map::MODE_WRITE);
			$this->assertFalse(isset($map[$testDomain]));
			$this->assertFalse(strpos(file_get_contents($sdbm), $testDomain));
		}

		public function testAsynchronousRemoval() {

			$testDomain = \Opcenter\Auth\Password::generate(16, 'a-z0-9') . '.test';
			$account = \Opcenter\Account\Ephemeral::create(['aliases.aliases.0' => $testDomain]);

			$afi = $account->getApnscpFunctionInterceptor();
			$this->assertTrue($afi->aliases_domain_exists($testDomain));
			$this->assertTrue($afi->aliases_remove_domain($testDomain));
			// pending synchronize changes
			$this->assertTrue($afi->aliases_domain_exists($testDomain));
			// add still permitted
			$afi->aliases_add_domain($testDomain, Web_Module::MAIN_DOC_ROOT);
			$afi->aliases_synchronize_changes();
			$this->assertTrue($afi->aliases_domain_exists($testDomain));
		}

		/**
		 * Promoting addon to primary drops DNS
		 */
		public function testDomainPromotion() {
			$ctx = $this->account;
			$adminApi = \apnscpFunctionInterceptor::factory(\Auth::context(TestHelpers::getAdmin()));
			try {
				$adminApi->admin_edit_site($ctx->getContext()->site, ['dns.provider' => 'powerdns']);
			} catch (\apnscpException $e) {
				$this->markTestSkipped("PowerDNS unavailable");
			}


			$testDomain = \Opcenter\Auth\Password::generate(16, 'a-z0-9') . '.test';
			$oldDomain = $ctx->getContext()->domain;
			$afi = $this->account->getApnscpFunctionInterceptor();
			$afi->aliases_add_domain($testDomain, Web_Module::MAIN_DOC_ROOT);
			$this->assertTrue($afi->aliases_synchronize_changes());
			$this->assertNotEmpty($afi->dns_get_records(null, 'A', $testDomain));
			$this->assertNotEmpty($afi->dns_get_records(null, 'A', $oldDomain));

			$afi->auth_change_domain($testDomain);
			$this->assertNotContains($oldDomain, $afi->web_list_domains());
			$this->assertNotEmpty($afi->dns_get_records(null, 'A', $testDomain));
			try {
				$this->assertEmpty($adminApi->dns_get_records(null, 'A', $oldDomain));
				$this->fail("Exception not generated");
			} catch(\apnscpException $e) {
				// side effect from EXCEPTION_LEVEL
			}
		}
	}

