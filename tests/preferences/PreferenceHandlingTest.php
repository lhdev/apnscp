<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class PreferenceHandlingTest extends TestFramework
    {
        public function testSameUserPreferences()
        {
        	// \Auth::profile() gets overwritten by \Opcenter\Account::installServices()
			$ctx = \TestHelpers::create(
				array_get(Definitions::get(), 'auth.site.domain')
			);
			$auth = \Auth::autoload()->setID($ctx->id);
			Auth::set_handler($auth);
            $prefs = \Preferences::factory($ctx);
            $prefs->unlock(apnscpFunctionInterceptor::factory($ctx));
            $key = uniqid('pref-key', true);
            $prefs[$key] = 'test';
            $prefs = null;
            $this->assertNotNull(\Preferences::get($key));
            \Preferences::forget($key);
            // Preferences are now dirty. Generating a new session ID
			// will invalidate this and cause fatal error on sync
			// sync now to avoid destructor taking care of it
            \Preferences::reload();
        }

    }

