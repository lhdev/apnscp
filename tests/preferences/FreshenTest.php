<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2021
 */


	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class FreshenTest extends TestFramework
	{
		private $saved;

		public function setUp()
		{
			parent::setUp();
			$this->saved = \session_id();
		}

		public function tearDown()
		{
			apnscpSession::restore_from_id($this->saved);
			parent::tearDown();
		}

		public function testPreferenceUpdate()
		{
			$ctx = \TestHelpers::create(
				array_get(Definitions::get(), 'auth.site.domain')
			);
			$auth = \Auth::autoload()->setID($ctx->id);
			Auth::set_handler($auth);
			$this->assertSame($ctx->id, \Auth::profile()->id);
			apnscpSession::restore_from_id($ctx->id, false);
			$card = UCard::init();
			$randPref = 'test.' . \Opcenter\Auth\Password::generate(16);
			$card->setPref($randPref, 'bar');
			$this->assertNotNull(Preferences::get($randPref));
			\apnscpFunctionInterceptor::factory($ctx)->common_set_preference($randPref, 'foo');
			//\Preferences::write();
			gc_collect_cycles();
			$this->assertSame($ctx->id, \Auth::profile()->id);
			$this->assertEquals(Preferences::get($randPref), 'foo');
			$this->assertSame($card->getPref($randPref), Preferences::get($randPref));
		}

	}
