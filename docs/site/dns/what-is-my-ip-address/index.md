---
title: "What is my IP address?"
date: "2016-01-22"
---

Your IP address, which is the location of your account, may be found within the [control panel](https://kb.apiscp.com/control-panel/logging-into-the-control-panel/) under **Account** > **Summary** > General >  IP Address.

[![ip-address-cp](images/ip-address-cp.png)](https://kb.apiscp.com/wp-content/uploads/2016/01/ip-address-cp.png)
