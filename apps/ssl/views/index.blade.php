<form method="post" data-toggle="validator" id="certificateForm">
	@if (\cmd('letsencrypt_debug'))
		<p class="alert-warning alert">
			Let's Encrypt is in debug mode. Any certificate issued while in debug mode is invalid.
		</p>
	@endif
	@if ($Page->certificateInstalled())
		@include('partials.certificate-issued')
	@elseif (\Util_Conf::call('ssl_permitted'))
		@include('partials.certificate-overview')
	@else
		<div class="row">
			<div class="col-12">
				<p class="alert alert-info ">
					You do not have SSL enabled for this account to use SSL. You may only generate certificate requests.
					@if (\Util_Conf::call('crm_configured'))
						<br/>SSL support may be
						added onto your account for a nominal rate of $2.50/month.
						<a class="ui-action ui-action-label ui-action-ticket"
						   href="{{\HTML_Kit::new_page_url_params('/apps/troubleticket', ['new' => 1, 'subjectid' => \Util_Conf::call('crm_get_subject_id_by_subject', 'billing')])}}">Open
							a ticket</a> to get started.
					@endif
				</p>
			</div>
		</div>
	@endif
	@includeWhen(\Util_Conf::call('ssl_permitted'), 'partials.create-certificate')
</form>