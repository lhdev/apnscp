@foreach ($value as $k => $v)
	@if (\is_array($v))
		@include('partials.scope-internals.type-array', ['value' => $v, 'name' => "$name" . "[$k]"])
		@continue
	@endif
	<div class="input-group mr-3 mb-3">
		@if (!is_int($k))
			<span class="input-group-addon">{{ $k }}</span>
		@endif
		<input {{ $attrs ?? '' }} type="text" name="{{ $name }}[{{ $k }}]" value="{{ is_bool($v) ? ($v ? 'true' : 'false') : $v  }}"
		       class="form-control" placeholder="{{ $v }}"/>
	</div>
@endforeach