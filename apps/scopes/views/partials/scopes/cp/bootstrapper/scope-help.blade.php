<p class="help">
	{{ $scope->getHelp() }}.
	<em>Use other Scopes before relying on this Scope</em>,
	which is a master set for configuring all Bootstrapper parameters. Direct Bootstrapper
	modifications do not support Scope undos.
	<br/><br/>
	@if ($scope->activeRole())
		This role information comes from
		<code>{{ \Opcenter\Admin\Bootstrapper::rolePath($scope->activeRole()) }}</code>
	@endif
</p>