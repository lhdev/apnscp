/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2017
 */

$(document).ready(function () {
	$("#vacation-toggle").change(
		function () {
			if ($(this).prop('checked')) {
				setTinyMode("design");
				$('#advancedToggle').prop('disabled', false);
				$("#vacation_msg").prop("disabled", false);
			} else {
				setTinyMode("disabled");
				$('#advancedToggle').prop('disabled', true);
				$('#advancedOptions').removeClass('show');
				$("#vacation_msg").prop("disabled", true);
			}
			return true;
		}
	).change();
	$('#nodupe').change(function () {
		if ($(this).prop('checked')) {
			$('#dupedelay').prop('disabled', false);
		} else {
			$('#dupedelay').prop('disabled', true);
		}
	}).change();

	$('#includemessage').change(function () {
		if ($(this).prop('checked')) {
			$('#salutation').prop('disabled', false);
		} else {
			$('#salutation').prop('disabled', true);
		}
	}).change();
	$('#modeSwitch').change(function () {
		$('#saveForm').click();
		return true;
	})
});

function setTinyMode(mode) {
	if (typeof tinymce === 'undefined' || typeof tinymce.activeEditor === 'undefined' || tinymce.activeEditor === null) {
		return false;
	}
	tinymce.activeEditor.setMode(mode);
}