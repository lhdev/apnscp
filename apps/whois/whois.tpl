<?php
?>
<!-- main object data goes here... -->
<form method="post">
	<div class="row">
		<label class="col-12">Domain</label>
		<fieldset class="form-group col-12 form-inline">
			<div class="btn-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-cloud"></i></span>
					<input type="text" id="domain" class="form-control" name="domain"
					       value="<?php print(isset($_POST['domain']) ? $_POST['domain'] :  \Util_Conf::login_domain()); ?>"/>
				</div>
				<button type="submit" class="btn btn-primary" value="Lookup">
					Lookup
				</button>
			</div>

		</fieldset>
	</div>
	<?php
        if ($Page->is_postback && !$Page->errors_exist()): ?>
	<h4>Results</h4>
	<code>
		<pre><?php print($Page->getWhoisRecord($_POST['domain']));?></pre>
	</code>
	<?php
        endif;
    ?>
</form>