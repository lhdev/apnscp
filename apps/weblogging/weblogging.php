<?php

	class Page extends Page_Container
	{
		/** postback function handler */
		public function __construct()
		{
			parent::__construct();
			$this->add_javascript('apnscp.hinted();', 'internal');
			$this->add_javascript('/apps/weblogging/weblogging.js');
			$this->add_javascript('var logs = {' . $this->generateHostJS($this->getHosts()) . '};', 'internal', false,
				true);
		}

		private function generateHostJS(array $hosts)
		{
			$hostjs = array();
			foreach ($hosts as $domain => $subdomains) {
				$hostjs[] = "'" . $domain . "': [" .
					"'" . join("','", array_keys($subdomains)) . "']";
			}

			return join(",", $hostjs);
		}

		public function getHosts()
		{
			$subdomains = $this->web_list_subdomains();
			$hosts = array('*' => array());
			$domains = $this->web_list_domains();
			for ($i = 0, $k = array_keys($subdomains), $n = sizeof($k); $i < $n; $i++) {
				$domain = '*';
				$subdomain = $k[$i];
				$pos = strpos($subdomain, '.');
				if (isset($domains[$subdomain]) || $pos > 0 && strrpos($subdomain, '.') == $pos) {
					// global fall-through
					$domain = $subdomain;
					$subdomain = '*';
				} else if ($pos !== false) {
					$domain = substr($subdomain, $pos + 1);
					$subdomain = substr($subdomain, 0, $pos);
				}
				if (!isset($hosts[$domain])) {
					$hosts[$domain] = array();
				}
				$hosts[$domain][$subdomain] = 1;

			}

			for ($i = 0, $k = array_keys($domains), $n = sizeof($k); $i < $n; $i++) {
				$domain = $k[$i];
				$hosts[$domain]['www'] = 1;
				ksort($hosts[$domain]);
			}

			return $hosts;
		}

		public function on_postback($params)
		{
			if (isset($params['add'])) {
				$this->logs_add_logfile($params['domain'], $params['subdomain'], $params['location']);
			} else if (isset($params['d'])) {
				list($subdomain, $domain) = explode(" ", base64_decode($params['d']));
				$this->logs_remove_logfile($domain, $subdomain);
			} else if (isset($params['download'])) {
				$prefix = $this->getAuthContext()->domain_fs_path();
				$path = $prefix . '/var/log/httpd/' . $params['download'];
				header('Content-disposition: attachment; filename="' . str_replace(array(';', '"'),
						'_', basename($params['download'])) . '"');
				header("Content-Type: application/octet-stream");
				header("Content-Transfer-Encoding: binary");
				header("Pragma: no-cache");
				header("Content-Length: " . filesize($path));
				header("Expires: 0");
				readfile($path);
			}
		}

		public function list_aliases()
		{
			$domains = array_keys($this->web_list_domains());
			asort($domains);

			return $domains;
		}

		public function list_subdomains()
		{
			$subdomains = $this->web_list_subdomains();
			// strip domain affinity from local subdomains
			foreach ($subdomains as $subdomain => $doc_root) {
				if (preg_match('/^([^\.]+)(?:\.(.+?)){2,}$/', $subdomain, $subcap)) {

					unset($subdomains[$subdomain]);
					$subdomains[$subcap[1]] = $doc_root;
				}
			}

			return array_merge(array('*' => '/var/www/html/', 'www' => '/var/www/html/'), $subdomains);
		}

		public function list_logfiles()
		{
			return $this->logs_list_logfiles();
		}
	}

?>
