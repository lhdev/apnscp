@php
	$status = $Page->getCronjobStatus();
@endphp
@includeWhen($status && !$Page->canSchedule(), 'partials.scheduling-disabled')
@includeWhen($status, 'schedule-form', ['jobs' => $Page->get_cronjob_data()])
@if ($status)
<hr/>
@endif
@includeWhen($Page->canSchedule(), 'partials.service-settings')

@component('theme::partials.app.modal')
	@slot('id')
		prgModal
	@endslot
	<i class="ui-action ui-action-run"></i><code class="cmd"></code>
	<hr />
	<div class="output_container text-monospace" style="overflow: auto; height: 150px;">
	</div>
@endcomponent