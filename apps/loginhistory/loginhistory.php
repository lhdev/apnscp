<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\loginhistory;

	class Page extends \Page_Container
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function getData()
		{
			$list = $this->auth_get_login_history(30);
			$lookups = array();
			for ($i = 0, $n = sizeof($list); $i < $n; $i++) {
				$entry = $list[$i];
				$list[$i]['ts'] = date('F j, Y g:i:s A T', $entry['ts']);
				if (!array_key_exists($entry['ip'], $lookups)) {
					$lookups[$entry['ip']] = $this->dns_gethostbyaddr_t($entry['ip']);
				}
				$list[$i]['host'] = $lookups[$entry['ip']];
			}
			$this->add_javascript('$("tr.entry").highlight()', 'internal', true, false);

			return $list;
		}
	}

	?>