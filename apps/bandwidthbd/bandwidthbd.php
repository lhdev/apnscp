<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\bandwidthbd;

	use Image_Color;
	use Page_Container;

	class Page extends Page_Container
	{
		const CACHE_KEY = 'bandwidthbd.bw';

		private $_spans;

		// bandwidth aggregate
		private $_bandwidth;

		// current span rendered as unix timestamp
		private $_activeSpan;

		// graph legend colors
		private $_legend;

		public function __construct()
		{
			parent::__construct();

			$this->add_javascript('$("tr.entry").highlight();', 'internal');
			$this->add_javascript('bandwidthbd.js');
			$this->add_css('bandwidthbd.css');
			$this->add_css('span.graph-key {display:block; height:20px; width:20px; border:1px solid #d6d6d6; margin:0 auto;}',
				'internal');
			$spans = $this->getSpans();
			$this->_activeSpan = array_shift($spans);
			$scripts = array('flot', 'flot.pie');
			call_user_func_array(array($this, 'init_js'), $scripts);
		}

		public function getSpans()
		{
			if (isset($this->_spans)) {
				return $this->_spans;
			}

			$key = 'bandwidthbd.spans';
			$cache = \Cache_Account::spawn();
			$spans = $cache->get($key);
			if ($spans) {
				return $spans;
			}

			$spans = array_reverse($this->bandwidth_get_cycle_periods());
			$cache->set($key, $spans, 21600);
			$this->_spans = $spans;

			return $spans;
		}

		public static function get_bandwidth($begin, $end)
		{
			$page = new Page();

			return $page->getBandwidth($begin, $end);
		}

		public function getBandwidth($begin, $end)
		{
			$hash = $this->_get_hash($begin, $end);
			if (isset($this->_bandwidth[$hash])) {
				return $this->_bandwidth[$hash];
			}

			$cache = \Cache_Account::spawn();
			$bw = $cache->get(self::CACHE_KEY);
			if ($bw === false) {
				$bw = array();
			}

			if (!isset($bw[$hash])) {
				$tmp = $this->bandwidth_get_by_date($begin, $end);
				// assign color
				$tmp = $this->_assignColors($tmp);

				$agg = $this->aggregate($tmp);
				// Sort BW
				$sort = function ($a, $b) {
					return ($a['in'] + $a['out']) <=> ($b['in'] + $b['out']);
				};
				$agg = $this->_assignColors($agg);
				uasort($agg, $sort);
				usort($tmp, $sort);
				// merge bandwidth items into categories
				foreach ($tmp as $t) {
					$svc = $t['svc_name'];
					$agg[$svc]['items'][] = $t;
				}
				$bw[$hash] = $agg;
			}
			$this->_bandwidth = $bw;
			$cache->set(self::CACHE_KEY, $bw, 21600);

			return $bw[$hash];
		}

		private static function _get_hash($begin, $end)
		{
			return crc32($begin . $end);
		}

		private function _assignColors($bw)
		{
			foreach ($bw as $key => $b) {
				$svc = $key;
				$extinfo = null;
				if (isset($b['svc_name'])) {
					// via bandwidth_get_by_date
					$svc = $b['svc_name'];
					$extinfo = $b['ext_info'];
				}

				$b['color'] = $this->_setColor($svc, $extinfo);
				$bw[$key] = $b;
			}

			return $bw;
		}

		/**
		 * Set hex color for item on graph
		 *
		 * @param string $item
		 * @param string $graph
		 * @param string $color hex color
		 */
		private function _setColor($item, $extinfo = null, $color = null)
		{
			if (is_null($color)) {
				list ($h, $s, $l) = Image_Color::randhsl();
				$color = Image_Color::hsl2hex($h, $s, $l);
			} else {
				if ($color[0] != '#' || strspn($color, 'abcdef0123456789', 1)) {
					return error("invalid hex value `%s'", $color);
				}
			}
			if ($extinfo) {
				$item = $item . ' ' . $extinfo;
			}
			$this->_legend[$item] = $color;

			return $color;
		}

		public function aggregate($bw)
		{
			if (!$bw) {
				return $bw;
			}
			$bwsum = array(
				'*' => array(
					'in'    => 0,
					'out'   => 0,
					'total' => 0
				)
			);
			foreach ($bw as $b) {
				$svc = $b['svc_name'];
				if (!isset($bwsum[$svc])) {
					$bwsum[$svc] = array(
						'in'    => 0,
						'out'   => 0,
						'total' => 0,
						'items' => array(),

					);
				}
				$in = $b['in'];
				$out = $b['out'];
				$bwsum[$svc]['in'] += $in;
				$bwsum[$svc]['out'] += $out;
				$bwsum[$svc]['total'] += $in + $out;
				$bwsum['*']['in'] += $in;
				$bwsum['*']['out'] += $out;
				$bwsum['*']['total'] += $in + $out;

			}

			return $bwsum;
		}

		public static function graph_range($begin, $end)
		{
			$cache = \Cache_Account::spawn();
			$bw = $cache->get(self::CACHE_KEY);
			if ($bw === false) {
				return array();
			}

			$hash = self::_get_hash($begin, $end);
			if (isset($bw[$hash])) {
				return $bw[$hash];
			}

			$page = new Page();
			$bw[$hash] = $page->getBandwidth($begin, $end);
			$cache->set(self::CACHE_KEY, $bw);

			return $bw[$hash];

		}

		public function getActiveSpan()
		{
			if (!isset($this->_activeSpan)) {
				$span = array_shift($this->getSpans());
				$this->_activeSpan = array(
					'begin' => (int)$span['begin'],
					'end'   => (int)$span['end']
				);
			}

			return $this->_activeSpan;
		}

		public function on_postback($params)
		{
			if (isset($params['span'])) {
				list($begin, $end) = explode(",", $params['span']);
				$this->_activeSpan = array(
					'begin' => (int)$begin,
					'end'   => (int)$end
				);
			}

		}

		public function getColor($item, $extinfo = null)
		{
			if ($extinfo) {
				$item = $item . ' ' . $extinfo;
			}
			if (isset($this->_legend[$item])) {
				return $this->_legend[$item];
			}
			warn('color not found for `%s\' in graph `%s\'',
				$item);

			return '#8a8a8a';
		}
	}