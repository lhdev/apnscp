$(document).ready(function () {
	$('ul.domain-heading .subdomain-heading').expandable('ul.subdomains');
	$('.ui-action-folder-browse').click(function () {
		var docroot = $(this).data('docroot'),
			hostname = $(this).data('hostname');

		apnscp.explorer({
			selected: docroot,
			filter: 'filter=file,0;chown,1;show,' + docroot,
			onSelect: function (file, b) {
				$('#selected_dir').text(file);
			}
		});
		$('#select_dir').click(function () {
			$('#modal').modal('hide');
			var url = apnscp.getUrl(), scheme = {
				path: $('#selected_dir').text().substring(docroot.length + 1),
				hostname: hostname
			};
			window.location.href = url.replace(/\?.*$/, '') + '?' + $.param(scheme);
			return false;
		});
		return false;
	});
});

var Personality = function () {
	var self = this;

	return {
		watch: function () {
			self.watch();
		}
	}
};

Personality.prototype.getSelectedPersonality = function () {
	return $('#personality').val();
};

Personality.prototype.getSelectedDirective = function () {
	return $('#directive').val();
};

Personality.prototype.getSelectedDirectiveValue = function () {
	return $('#directive-val').val();
};

Personality.prototype.watch = function () {
	var self = this;
	$('#add-form').bind('change', function (ev) {
		var $el = $(ev.target);
		switch ($el.attr('id')) {
			case 'personality':
				apnscp.call_app(null, 'getDirectives', [$el.val()], {dataType: 'json'}).then(function (data) {
					var $html = new Array();
					for (var i in data) {
						$html.push('<option value="' + data[i] + '">' + data[i] + '</option>');
					}
					self.set('directive', $($html.join(""))).done(
						function () {
							$('#directive').trigger('change');
						}
					);
				});
				break;
			case 'directive':
				apnscp.call_app(null, 'getDescription', [self.getSelectedPersonality(), $el.val()], {dataType: 'json'}).then(function (data) {
					self.set('help', $('<span>' + data + '</span>'));
				});
				break;
			default:
				return true;
		}
		return false;
	}).bind('submit', function (ev) {
		var personality = self.getSelectedPersonality(),
			dir = self.getSelectedDirective(),
			val = self.getSelectedDirectiveValue(),
			directive = self.createDirective(personality, dir, val),
			$ajaxIndicator = $('#ajax-image');
		$ajaxIndicator.show().removeClass('ui-ajax-error ui-ajax-success').addClass('ui-ajax-loading');
		$('#error-message').empty();
		apnscp.cmd('personality_verify', [dir, val, personality]).pipe(function (data, status, res) {
			//this.Reject(res, status, "test");
			self.add(directive);
			$ajaxIndicator.removeClass('ui-ajax-loading').addClass('ui-ajax-success');
			setTimeout(function () {
				$ajaxIndicator.fadeOut('slow');
			}, 1000);
		}, function (xhr) {
			var error = $.parseJSON(xhr.responseText);
			$ajaxIndicator.removeClass('ui-ajax-loading').addClass('ui-ajax-error');
			for (var i in error['errors']) {
				$('#error-message').append($('<span class="error-item">' + error['errors'][i] + '</span>'));
			}
		});
		return false;
	});

	$('#personality').change();

	$('#htaccess').bind('click', function (ev) {
		var $el = $(ev.target), $parent = $el.parent(), $this = $(ev.target);
		if ($this.hasClass('ui-action-delete')) {
			// send as line number
			var lineno = parseInt($this.attr('rel')) + 1;
			self.remove(lineno);
			return false;
		} else if ($this.hasClass('ui-action-edit')) {
			var lineno = parseInt($this.attr('rel')) + 1;
			self.editable(lineno);
			return false;
		}

		return true;

	});
};

Personality.prototype.editable = function (lineno) {
	var $el = $('#htaccess > li[data-line=' + --lineno + ']'),
		text = $el.find(':hidden').val().trim(), $editableEl;

	if (-1 !== text.indexOf("\n")) {
		$editableEl = $('<textarea>').attr({
			name: 'lines[]',
			rows: (text.match(/\n/g) || []).length,
			'class': 'editable form-control w-100'
		}).text(text);
	} else {
		$editableEl = $('<input />').attr({
			name: 'lines[]',
			type: 'text',
			value: text,
			'class': 'editable form-control'
		});
	}
	$el.empty().append($editableEl);
	return false;
};

Personality.prototype.remove = function (lineno) {
	var $el = $('#htaccess > li[data-line=' + --lineno + ']');

	$el.fadeOut('fast', function () {
		$el.remove();
	});
};

Personality.prototype.add = function (what, lineno) {
	if (lineno == undefined) {
		lineno = $('#htaccess > li').length - 1;
	}
	$('#htaccess > li').eq(lineno).after($(what));

};


/**
 * Create a new directive object
 *
 * @param directive
 * @param val
 */
Personality.prototype.createDirective = function (personality, directive, val) {
	return '<li class="directive p-new">' + directive + ' ' + val + this.createInputObject(directive + ' ' + val) + '</li>';
};

Personality.prototype.createInputObject = function (args) {
	return '<input type="hidden" name="lines[]" value="' + apnscp.escapeHTML(args) + '" />';
};

Personality.prototype.set = function (what, data) {
	return $('#' + what).empty().append(data).promise();
};

var p = new Personality();
p.watch();