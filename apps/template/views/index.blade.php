@extends('theme::layout')
@section('content')
	<p class="">
		Hello! I am a <mark>@@section('content')</mark> section and must appear using routing!
		Check out my <a href="/apps/template/hello/world">hello world</a> route.
	</p>
	<p>
		Onboard tours are available by setting <mark>tour</mark> in <code>application.yml</code>. Check
		out the sample in <code>/apps/template/application.yml</code>.

		<a href="/apps/template/tour">Click here</a> to enable the tour.
		By default onboard tours appear on first app access. Tours never display automatically
		when impersonating an account.
	</p>
	@foreach (['danger' => 'error', 'warning' => 'warn', 'info' => 'info', 'success' => 'success'] as $class => $macro)
		<div class="alert alert-{{ $class }}">
			Hello, I am a {{ $class }}-grade message! You can generate me by
			using <code>{{ $macro }}("Some message");</code>
		</div>
	@endforeach

	<div class="d-flex align-items-stretch">
		<label class="custom-checkbox custom-control">
			<input type="checkbox" name="test" class="custom-control-input" value="Test"/>
			<span class="custom-control-indicator"></span>
			Sample custom checkbox
		</label>

		<label class="custom-switch custom-control">
			<input type="checkbox" name="test" class="custom-control-input" value="Test"/>
			<span class="custom-control-indicator"></span>
			Sample custom switch
		</label>

		<label class="custom-radio custom-control">
			<input type="checkbox" name="test" class="custom-control-input" value="Test"/>
			<span class="custom-control-indicator"></span>
			Sample custom radio
		</label>

		<label class="custom-checkbox custom-control">
			<input type="checkbox" name="test" class="custom-control-input" DISABLED CHECKED value="Test"/>
			<span class="custom-control-indicator"></span>
			Sample custom disabled checkbox
		</label>
	</div>
@stop

