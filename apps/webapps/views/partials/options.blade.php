@include('partials.options.global', ['meta' => $app->getAppMeta()])
@if ($Page->hasOptions())
	@includeIf('@webapp(' . ($_GET['app'] ?? strtolower($app->getModuleName())) . ')::options', ['meta' => $app->getAppMeta()])
	@includeIf('@webapp(' . $app->getAppFamily() . ')::options', ['meta' => $app->getAppMeta()])
@endif