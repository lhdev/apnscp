@if ($mode === \Module\Support\Webapps\App\Installer::INSTALLING_VERSION)
	<label class="badge badge-warning" id="installerCheck">
		<i class="ui-ajax-indicator ui-ajax-loading"></i>
		installing
	</label>
@elseif ($mode === null)
	<label class="badge badge-info">
		<i class="fa fa-lock"></i>
		latest
	</label>
@elseif ($mode > 0)
	<label class="badge badge-success">
		latest
	</label>
@elseif ($mode < 0)
	<label class="badge badge-default">
		unknown
	</label>
@else
	<label class="badge badge-danger">
		update
	</label>
@endif