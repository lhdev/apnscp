@component('theme::partials.app.modal')
	@slot('title')
		Suspend sites
	@endslot
	@slot('id')
		suspendModal
	@endslot
	<div>
		<p>
			Following sites will be suspended: <b class="site-list"></b>
		</p>
		<label>
			Suspension reason
		</label>
		<textarea name="custom-message" id="reason" disabled class="form-control w-100" rows="5"></textarea>
		<label class="custom-control custom-checkbox mt-1 mb-0">
			<input name="enable" type="checkbox" class="custom-control-input" value="1" id="enableReason" />
			<span class="custom-control-indicator"></span>
			<span>
				Specify suspension reason
			</span>
		</label>
	</div>
	@slot('buttons')
		<button type="submit" name="suspend" class="btn btn-primary ajax-wait ui-action ui-action-label ui-action-disable">
			<i class="fa fa-pulse d-none"></i>
			Suspend Site
		</button>
		<p class="ajax-response"></p>
	@endslot
@endcomponent