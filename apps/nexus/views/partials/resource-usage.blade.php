<div class="resource-group ml-auto text-right d-md-inline-flex align-self-stretch d-flex" data-type="domain"
     data-value="{{ $meta['siteinfo']['domain'] }}"
     style="font-size: smaller;">
	@if ($showResources)
		<div class="d-inline-flex flex-column mr-2">
			@include('partials.resource-widget', [
				'title'   => _('CPU'),
				'used'    => $cgroup['cpu']['used'] ?? 0,
				'unit'    => 'sec',
				'srcunit' => 'sec',
				'limit'   => $cgroup['cpu']['threshold'] ?? 0
			])
		</div>
		<div class="d-inline-flex flex-column mr-2">
			@include('partials.resource-widget', [
				'title'   => _('Disk IO'),
				'used'    => ($cgroup['io']['read'] ?? 0) + ($cgroup['io']['write'] ?? 0),
				'unit'    => 'MB',
				'srcunit' => 'KB',
				'limit'   => ($cgroup['io']['threshold'] ?? 0)*1024
			])
		</div>
		<div class="d-inline-flex flex-column mr-2">
			@include('partials.resource-widget', [
				'title'   => _('Memory'),
				'used'    => $cgroup['memory']['used'] ?? 0,
				'unit'    => 'MB',
				'srcunit' => 'KB',
				'limit'   => ($cgroup['memory']['threshold'] ?? 0)
			])
		</div>
	@endif
	<div class="d-inline-flex flex-column mr-2">
		@include('partials.resource-widget', [
			'title'   => _('Storage'),
			'used'    => $storage['qused'] ?? 0,
			'unit'    => 'MB',
			'srcunit' => 'KB',
			'limit'   => $storage['qhard'] ?? 0
		])
	</div>
	@if ($showInodes)
		<div class="d-inline-flex flex-column mr-2">
			@include('partials.resource-widget', [
				'title'   => _('Inodes'),
				'used'    => $storage['fused'] ?? 0,
				'unit'    => '',
				'srcunit' => '',
				'limit'   => $storage['fhard'] ?? 0
			])
		</div>
	@endif

	<div class="d-inline-flex flex-column">
		@include('partials.resource-widget', [
			'title'   => _('Bandwidth'),
			'used'    => $bandwidth['sum'] ?? 0,
			'unit'    => 'GB',
			'srcunit' => 'B',
			'limit'   => $bandwidth['threshold'] ?? 0
		])
	</div>
</div>