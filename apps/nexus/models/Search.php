<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace apps\nexus\models;

use Template\Searchlet;

class Search extends Searchlet
{
	public function specs(): array
	{
		return [
			'domain'     => 'Domain',
			'addon'      => 'Addon Domain',
			'admin-user' => 'Admin Username',
			'site-id'    => 'Site ID',
			'email'      => 'Email',
			'invoice'    => 'Billing ID'
		];
	}

	public function filterClass(): string
	{
		return 'filter-accounts';
	}


}

