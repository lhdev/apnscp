<table class="table table-striped thead-dark w-100">
	<thead>
	<tr class="">
		<th colspan="2">
			<h3 class="mb-0 align-items-center d-flex">
				<label class="custom-switch custom-control mb-0 pl-0 mr-0 font-weight-normal">
					<input type="checkbox" class="master-select custom-control-input"/>
					<span class="custom-control-indicator my-auto"></span>
					{{ $group['name'] }}
				</label>
			</h3>
		</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($group['items'] as $filter => $data)
		<tr>
			<td colspan="2">
				<label class="custom-switch custom-control mb-0 pl-0 mr-0">
					<input type="checkbox" name="filter[]" class="custom-control-input filter-control"
					       value="{{ $filter }}" @if ($Page->filterEnabled($filter)) CHECKED @endif />
					<span class="custom-control-indicator"></span>
					<b>{{ $data['name'] }}</b>
				</label>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="align-content-around d-flex">
								<span class="d-flex mr-1">
									{{ $data['help'] }}
								</span>
					<a href="{{ $Page->docLink($filter) }}" target="pagespeedhelp"
					   class="d-flex ml-auto text-light font-italic">{{ $filter }}</a>
				</div>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>