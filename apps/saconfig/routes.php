<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */

	use Illuminate\Support\Facades\Route;
	use Lararia\Http\JsonResponse;

	Route::get('', 'Page@index');

	Route::get('tip/{tip}', static function ($tip, Page $Page) {
		return $Page->getTip($tip);
	});

	Route::post('', static function (Page $Page) {
		return $Page->route(request());
	});

	Route::post('global', static function (\Illuminate\Http\Request $request, Page $Page) {
		if ($request->has('global_threshold')) {
			\apnscpFunctionInterceptor::init()->
				call('spamfilter_set_delivery_threshold', [(float)$request->post("global_threshold")]);
		}
		return $Page->index();
	});

