<div class="row">
	<div class="col-12">
		<h4 class="step">Thresholds and Tagging</h4>
		<ol class="pl-0 ml-3">
			<li class="">
				<label class="form-inline">
					<span>
						A <a href="{{ HTML_Kit::page_url() }}/tip/score" class="popover-trigger" id="score">score</a> of
					</span>
					<input type="number" step="any" class="form-control mx-2" name="spam_score"
					       value="{{ $spam_score }}" size="4" maxlength="4"/>
					<span>
						or above constitutes spam.
					</span>
				</label>
			</li>

			<li class="">
				<label class="form-control-static">
					How shall SpamAssassin <a href="{{ HTML_Kit::page_url() }}/tip/tag" class="popover-trigger" id="tag">tag</a> the subject
					lines of
					a verified spam?
				</label>

				<ul class="list-unstyled">
					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="tagging_method"
							       value="prefix_spam" class="custom-control-input"
							       @if ($tagging_method == "prefix_spam") CHECKED @endif />

							<span class="custom-control-indicator"></span>

							<span>
								Prefix all spams with &quot;[SPAM] (<em>spam score</em>)&quot;
								 <b class="badge badge-info text-uppercase">recommended</b>
							</span>
						</label>
					</li>

					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="tagging_method"
							       value="prefix_traditional" class="custom-control-input"
							       @if ($tagging_method == "prefix_traditional") CHECKED @endif />

							<span class="custom-control-indicator"></span>
							<span>
								Prefix all spams with &quot;[SPAM]&quot;
							</span>
						</label>
					</li>

					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="tagging_method"
							       value="prefix_none" class="custom-control-input"
							       @if ($tagging_method == "prefix_none") CHECKED @endif />
							<span class="custom-control-indicator"></span>
							<span>
								Perform no modification to the message
							</span>
						</label>
					</li>
					<li>
						<label class="custom-control custom-radio mb-0">
							<input type="radio" name="tagging_method"
							       value="prefix_custom" class="custom-control-input"
							       @if ($tagging_method == "prefix_custom") CHECKED @endif />
							<span class="custom-control-indicator"></span>
							<span>
								<a href="{{ HTML_Kit::page_url() }}/tip/user-defined-tag" class="popover-trigger" id="user-defined-tag">User-defined</a>:
								<input type="text" class="form-control" name="tagging_method_txt"
							       value="{{ old("tagging_method_txt") }} "/>
							</span>
						</label>
					</li>
				</ul>
			</li>
		</ol>
	</div>
</div>