<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace apps\dns\models;

use Template\Searchlet;

class Search extends Searchlet
{
	public function specs(): array
	{
		return [
			'hostname'  => 'Subdomain',
			'rr'        => 'RR',
			'ttl'       => 'TTL',
			'parameter' => 'Parameter'
		];
	}

	public function operations(): array
	{
		return [];
	}

	public function filterLabel(): string
	{
		return 'Filter DNS';
	}


	public function filterClass(): string
	{
		return 'dns-filter';
	}

	public function formAction(): ?string
	{
		// inherited
		return null;
	}


}

